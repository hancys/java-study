package com.hancy.springcloud.alibaba.domain;

import lombok.Data;

/**
 * @author Tomorrow
 * @create 2021-12-12 22:30
 */
@Data
public class Storage {

    private Long id;

    /**
     * 产品id
     */
    private Long productId;

    /**
     * 总库存
     */
    private Integer total;

    /**
     * 已用库存
     */
    private Integer used;

    /**
     * 剩余库存
     */
    private Integer residue;
}
