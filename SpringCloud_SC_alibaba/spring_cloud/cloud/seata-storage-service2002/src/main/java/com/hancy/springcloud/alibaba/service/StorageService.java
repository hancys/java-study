package com.hancy.springcloud.alibaba.service;


/**
 * @author Tomorrow
 * @create 2021-12-12 22:30
 */
public interface StorageService {
    /**
     * 扣减库存
     */
    void decrease(Long productId, Integer count);
}
