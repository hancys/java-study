package com.hancy.springcloud.alibaba.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Tomorrow
 * @create 2021-12-12 22:30
 */
@Configuration
@MapperScan({"com.hancy.springcloud.alibaba.dao"})
public class MyBatisConfig {
}
