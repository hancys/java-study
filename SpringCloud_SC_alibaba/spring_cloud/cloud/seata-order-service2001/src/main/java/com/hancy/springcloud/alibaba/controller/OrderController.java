package com.hancy.springcloud.alibaba.controller;

import com.hancy.springcloud.alibaba.domain.CommonResult;
import com.hancy.springcloud.alibaba.domain.Order;
import com.hancy.springcloud.alibaba.service.OrderService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author Tomorrow
 * @create 2021-12-12 22:00
 */
@RestController
public class OrderController {
    @Resource
    private OrderService orderService;


    @GetMapping("/order/create")
    public CommonResult create(Order order)
    {
        orderService.create(order);
        return new CommonResult(200,"订单创建成功");
    }
}
