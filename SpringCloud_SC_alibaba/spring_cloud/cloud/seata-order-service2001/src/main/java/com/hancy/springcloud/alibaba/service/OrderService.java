package com.hancy.springcloud.alibaba.service;

import com.hancy.springcloud.alibaba.domain.Order;

/**
 * @author Tomorrow
 * @create 2021-12-12 22:00
 */
public interface OrderService {
    /**
     * 创建订单
     * @param order
     */
    void create(Order order);
}
