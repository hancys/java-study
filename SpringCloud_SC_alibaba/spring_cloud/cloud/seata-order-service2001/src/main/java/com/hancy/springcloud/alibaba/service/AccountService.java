package com.hancy.springcloud.alibaba.service;

import com.hancy.springcloud.alibaba.domain.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

/**
 * @author Tomorrow
 * @create 2021-12-12 22:00
 */
@FeignClient(value = "seata-account-service")
public interface AccountService {
    /**
     * 账户扣减
     * @param userId
     * @param money
     * @return
     */
    @PostMapping(value = "/account/decrease")
    CommonResult decrease(@RequestParam("userId") Long userId, @RequestParam("money") BigDecimal money);
}
