package com.hancy.springcloud.service;

/**
 * @author Tomorrow
 * @create 2021-12-07 21:30
 */
public interface IMessageProvider {
    public String send();
}
