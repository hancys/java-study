package com.hancy.springcloud.service.impl;

import com.hancy.springcloud.dao.PaymentDao;
import com.hancy.springcloud.entities.Payment;
import com.hancy.springcloud.service.PaymentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author Tomorrow
 * @create 2021-11-24 21:54
 */
@Service
public class PaymentServiceImpl implements PaymentService {

    @Resource
    private PaymentDao paymentDao;

    @Override
    public int create(Payment payment) {
        return paymentDao.create(payment);
    }

    @Override
    public Payment getPaymentById(Long id) {
        return paymentDao.getPaymentById(id);
    }
}
