package com.hancy.springcloud.alibaba.service;

import com.hancy.springcloud.entities.CommonResult;
import com.hancy.springcloud.entities.Payment;
import org.springframework.stereotype.Component;

/**
 * @author Tomorrow
 * @create 2021-12-12 17:40
 */
@Component
public class PaymentFallbackService implements PaymentService
{
    @Override
    public CommonResult<Payment> paymentSQL(Long id)
    {
        return new CommonResult<>(44444,"服务降级返回,---PaymentFallbackService",new Payment(id,"errorSerial"));
    }
}
