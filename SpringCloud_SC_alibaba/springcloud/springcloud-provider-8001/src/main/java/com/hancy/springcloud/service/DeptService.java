package com.hancy.springcloud.service;

import com.hancy.springcloud.pojo.Dept;

import java.util.List;

/**
 * @author Tomorrow
 * @create 2021-09-08 15:40
 */
public interface DeptService {
    /**
     * 添加一个部门
     * @param dept
     * @return
     */
    boolean addDept(Dept dept);

    /**
     * 通过id查询部门
     * @param id
     * @return
     */
    Dept queryById(Long id);

    /**
     * 查询所有部门
     * @return
     */
    List<Dept> queryAllDept();
}
