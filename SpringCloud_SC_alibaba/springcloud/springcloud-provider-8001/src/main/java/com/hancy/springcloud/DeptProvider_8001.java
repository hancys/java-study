package com.hancy.springcloud;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author Tomorrow
 * @create 2021-09-08 15:47
 */
@MapperScan("com.hancy.springcloud.dao")
@SpringBootApplication
@EnableEurekaClient
@EnableCircuitBreaker //�۶�
public class DeptProvider_8001 {
    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(DeptProvider_8001.class, args);
        String[] allBeanNames = run.getBeanDefinitionNames();
        for(String beanName : allBeanNames) {
            System.out.println(beanName);
        }
    }
}
