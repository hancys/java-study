package com.hancy.springcloud.service;

import com.hancy.springcloud.dao.DeptDao;
import com.hancy.springcloud.pojo.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


import java.util.List;

/**
 * @author Tomorrow
 * @create 2021-09-08 15:41
 */
@Service
public class DeptServiceImpl implements DeptService{

    @Autowired
    private DeptDao deptDao;

    @Override
    public boolean addDept(Dept dept) {
        return deptDao.addDept(dept);
    }

    @Override
    public Dept queryById(Long id) {
        return deptDao.queryById(id);
    }

    @Override
    public List<Dept> queryAllDept() {
        return deptDao.queryAllDept();
    }
}
