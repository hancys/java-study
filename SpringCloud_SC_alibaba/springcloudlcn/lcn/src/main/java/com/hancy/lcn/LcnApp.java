package com.hancy.lcn;

import com.codingapi.txlcn.tm.config.EnableTransactionManagerServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Tomorrow
 * @create 2021-09-15 14:01
 */

@SpringBootApplication
@EnableTransactionManagerServer
public class LcnApp {
    public static void main(String[] args) {
        SpringApplication.run(LcnApp.class,args);
    }
}
