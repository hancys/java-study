package com.hancy.clienta;

import com.codingapi.txlcn.tc.config.EnableDistributedTransaction;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Tomorrow
 * @create 2021-09-15 14:20
 */
@SpringBootApplication
@EnableDistributedTransaction
@MapperScan("com.hancy.clienta.dao")
public class ClientAApp {
    public static void main(String[] args) {
        SpringApplication.run(ClientAApp.class,args);
    }
}
