package com.hancy.clienta.controller;

import com.hancy.clienta.bean.Account;
import com.hancy.clienta.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Tomorrow
 * @create 2021-09-15 14:35
 */
@RestController
public class ClientAController {
    @Autowired
    private AccountService accountService;

    @GetMapping("add")
    public String subtrack(@RequestParam("money") int money, @RequestParam("username") String username){
        Account account = new Account();
        account.setMoney(money);
        account.setUsername(username);
        return accountService.setById(account);
    }
}
