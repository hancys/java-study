package com.hancy.clienta.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * @author Tomorrow
 * @create 2021-09-15 14:31
 */
public interface AccountMapper {
    @Update("update account set money = money + #{money} where username = #{username}")
    public int setById(@Param("money") Integer money, @Param("username") String username);
}
