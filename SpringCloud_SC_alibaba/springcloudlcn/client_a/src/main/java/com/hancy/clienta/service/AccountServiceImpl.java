package com.hancy.clienta.service;

import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import com.hancy.clienta.bean.Account;
import com.hancy.clienta.dao.AccountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Tomorrow
 * @create 2021-09-15 14:33
 */
@Service
public class AccountServiceImpl implements AccountService{

    @Autowired
    private AccountMapper accountMapper;

    @Override
    @LcnTransaction
    @Transactional
    public String setById(Account account) {
        int r = accountMapper.setById(account.getMoney(),account.getUsername());
        if (r>0){
            return "success";
        }
        return "error";
    }
}
