package com.hancy.clienta.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Tomorrow
 * @create 2021-09-15 14:28
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Accessors(chain = true)
public class Account implements Serializable {
    private Integer id;
    private String username;
    private Integer money;
    private Date CreateTime;
}
