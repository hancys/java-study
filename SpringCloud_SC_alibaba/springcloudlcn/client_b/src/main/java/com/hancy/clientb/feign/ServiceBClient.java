package com.hancy.clientb.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Tomorrow
 * @create 2021-09-15 14:50
 */
@FeignClient(value = "service-a")
public interface ServiceBClient {

    @GetMapping("add")
    String save(@RequestParam("money") int money, @RequestParam("username") String username);
}
