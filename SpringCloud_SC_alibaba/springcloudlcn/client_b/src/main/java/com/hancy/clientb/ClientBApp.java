package com.hancy.clientb;

import com.codingapi.txlcn.tc.config.EnableDistributedTransaction;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author Tomorrow
 * @create 2021-09-15 14:45
 */
@EnableFeignClients
@SpringBootApplication
@EnableDistributedTransaction
@MapperScan("com.hancy.clientb.dao")
public class ClientBApp {
    public static void main(String[] args) {
        SpringApplication.run(ClientBApp.class,args);
    }
}
