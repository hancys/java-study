package com.hancy.clientb.service;

import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import com.hancy.clientb.bean.Account;
import com.hancy.clientb.dao.AccountMapper;
import com.hancy.clientb.feign.ServiceBClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Tomorrow
 * @create 2021-09-15 14:33
 */
@Service
public class AccountServiceImpl implements AccountService{

    @Autowired
    private AccountMapper accountMapper;
    @Autowired
    private ServiceBClient serviceBClient;

    @Override
    @LcnTransaction
    @Transactional
    public String setById(Account account) {
        int r = accountMapper.setById(account.getMoney(),account.getUsername());
        if (r>0){
            String result = serviceBClient.save(account.getMoney(),account.getUsername());
            return "success";
        }
        return "error";
    }
}
