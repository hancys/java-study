package com.hancy.clientb.controller;

import com.hancy.clientb.bean.Account;
import com.hancy.clientb.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Tomorrow
 * @create 2021-09-15 14:35
 */
@RestController
public class ClientBController {
    @Autowired
    private AccountService accountService;

    @GetMapping("update")
    public String subtrack(@RequestParam("money") int money, @RequestParam("username") String username){
        Account account = new Account();
        account.setMoney(money);
        account.setUsername(username);
        return accountService.setById(account);
    }
}
