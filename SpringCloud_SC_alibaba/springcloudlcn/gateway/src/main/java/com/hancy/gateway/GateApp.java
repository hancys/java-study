package com.hancy.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Tomorrow
 * @create 2021-09-15 11:27
 */
@SpringBootApplication
public class GateApp {
    public static void main(String[] args) {
        SpringApplication.run(GateApp.class,args);
    }
}
