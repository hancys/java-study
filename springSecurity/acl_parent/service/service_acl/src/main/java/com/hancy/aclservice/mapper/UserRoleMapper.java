package com.hancy.aclservice.mapper;

import com.hancy.aclservice.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Tomorrow
 * @create 2021-08-27 20:30
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
