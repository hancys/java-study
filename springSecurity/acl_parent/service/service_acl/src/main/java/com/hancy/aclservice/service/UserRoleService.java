package com.hancy.aclservice.service;

import com.hancy.aclservice.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Tomorrow
 * @create 2021-08-27 20:24
 */
public interface UserRoleService extends IService<UserRole> {

}
