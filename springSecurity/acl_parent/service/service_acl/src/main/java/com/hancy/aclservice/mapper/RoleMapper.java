package com.hancy.aclservice.mapper;

import com.hancy.aclservice.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Tomorrow
 * @create 2021-08-27 20:33
 */
public interface RoleMapper extends BaseMapper<Role> {

}
