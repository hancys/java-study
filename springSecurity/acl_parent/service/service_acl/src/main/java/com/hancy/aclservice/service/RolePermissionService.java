package com.hancy.aclservice.service;

import com.hancy.aclservice.entity.RolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色权限 服务类
 * </p>
 *
 * @author Tomorrow
 * @create 2021-08-27 20:26
 */
public interface RolePermissionService extends IService<RolePermission> {

}
