package com.hancy.aclservice.mapper;

import com.hancy.aclservice.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author Tomorrow
 * @create 2021-08-27 20:31
 */
public interface UserMapper extends BaseMapper<User> {

}
