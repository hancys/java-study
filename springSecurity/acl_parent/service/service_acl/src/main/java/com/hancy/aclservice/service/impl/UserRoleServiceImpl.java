package com.hancy.aclservice.service.impl;

import com.hancy.aclservice.entity.UserRole;
import com.hancy.aclservice.mapper.UserRoleMapper;
import com.hancy.aclservice.service.UserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Tomorrow
 * @create 2021-08-27 20:23
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

}
