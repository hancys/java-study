package com.hancy.aclservice.mapper;

import com.hancy.aclservice.entity.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色权限 Mapper 接口
 * </p>
 *
 * @author Tomorrow
 * @create 2021-08-27 20:32
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}
