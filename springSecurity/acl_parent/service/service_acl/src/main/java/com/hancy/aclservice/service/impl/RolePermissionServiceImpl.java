package com.hancy.aclservice.service.impl;

import com.hancy.aclservice.entity.RolePermission;
import com.hancy.aclservice.mapper.RolePermissionMapper;
import com.hancy.aclservice.service.RolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限 服务实现类
 * </p>
 *
 * @author Tomorrow
 * @create 2021-08-27 20:22
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

}
