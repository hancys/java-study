package com.han.Demo2;

import com.han.utils.JdbcUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Tomorrow
 * @create 2021-08-07 0:56
 */
public class Transaction {
    public static void main(String[] args) {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
            conn = JdbcUtils.getConnection();

            conn.setAutoCommit(false);

            String sql = "update gongzi set jiben = jiben + 100 where id = ?";

            st = conn.prepareStatement(sql);
            st.setInt(1,28);
            st.executeUpdate();

            int x = 1 /0 ;

            String sql2 = "update gongzi set jiben = jiben - 100 where id = ?";

            st = conn.prepareStatement(sql2);
            st.setInt(1,29);
            st.executeUpdate();

            System.out.println("dfdfdffdfdf");
            conn.commit();

        } catch (ArithmeticException throwables) {
            try {
                conn.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            //throwables.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JdbcUtils.release(conn,st,rs);
        }
    }
}
