package com.han;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Driver;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author Tomorrow
 * @create 2021-08-06 20:08
 */
public class JDBCTest {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        //1、加载驱动
        Class.forName("com.mysql.jdbc.Driver");
        //2、用户信息 url
        String url = "jdbc:mysql://localhost:3306/gongzi?useUnicode=true&characterEncoding=utf8&useSSL=true";
        String username = "root";
        String password = "rootroot";
        //3、连接成功、数据库对象
        java.sql.Connection connection = DriverManager.getConnection(url, username, password);
        //4、执行SQL
        Statement statement = connection.createStatement();
        //5、执行SQL对象去执行SQL 可能存在结果 查看结果
        String sql = "select * from gongzi";
        ResultSet resultSet = statement.executeQuery(sql);

        while (resultSet.next()){
            System.out.println(resultSet.getInt("id"));
        }
        //6、释放连接
        resultSet.close();
        statement.close();
        connection.close();
    }
}
