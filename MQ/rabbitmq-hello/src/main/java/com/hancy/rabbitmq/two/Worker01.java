package com.hancy.rabbitmq.two;

import com.hancy.rabbitmq.utils.RabbitMqUtils;
import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;

/**
 * @author Tomorrow
 * @create 2021-12-23 20:29
 */
public class Worker01 {

    //队列名称
    public static final String QUEUE_NAME = "hello";

    //接受消息
    public static void main(String[] args) throws Exception {
        Channel channel = RabbitMqUtils.getChannel();

        //消息的接受
        DeliverCallback deliverCallback = (consumerTag,message) -> {
            System.out.println("接收到的消息" + new String(message.getBody()));
        };

        //消息接收被取消是
        CancelCallback cancelCallback = consumerTag -> {
            System.out.println("消息被取消接口");
        };
        /**
         * 消费者接收消息
         * 1、消费哪个队列
         * 2、消费成功之后是否自动应答true 代表自动应答 false代表手动应答
         * 3、消费者未成功消费的回调
         * 4、消费者取消消费的回调
         */
        System.out.println("C1等待接收");
        channel.basicConsume(QUEUE_NAME,true,deliverCallback,cancelCallback);

    }

}
