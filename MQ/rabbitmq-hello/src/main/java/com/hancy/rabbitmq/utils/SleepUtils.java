package com.hancy.rabbitmq.utils;

/**
 * @author Tomorrow
 * @create 2021-12-23 21:33
 */
public class SleepUtils {
    public static void sleep(int second){
        try {
            Thread.sleep(1000*second);
        } catch (InterruptedException _ignored) {
            Thread.currentThread().interrupt();
        }
    }
}
