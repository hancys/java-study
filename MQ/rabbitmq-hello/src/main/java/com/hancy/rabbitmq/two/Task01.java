package com.hancy.rabbitmq.two;

import com.hancy.rabbitmq.utils.RabbitMqUtils;
import com.rabbitmq.client.Channel;

import java.util.Scanner;

/**
 * @author Tomorrow
 * @create 2021-12-23 20:41
 */
public class Task01 {

    //队列名称
    public static final  String  QUEUE_NAME = "hello";

    //发送大量消息
    public static void main(String[] args) throws Exception {

        Channel channel = RabbitMqUtils.getChannel();

        /**
         * 产生一个队列
         * 1、队列名称
         * 2、队列中消息是否持久化，默认在内存中
         * 3、该队列是否只供一个消费者进行消费，是否进行消费共享 true可以多个消费者消费
         * 4、是否自动删除，最后一个消费者断开连接后是否自动删除
         * 5、其他参数
         */
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);
        //发消息
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()){
            String message = scanner.next();
            /**
             * 发送一个参数
             * 1、发送那个交换机
             * 2、路由的key
             * 3、其他参数信息
             * 4、发送消息的消息体
             */
            channel.basicPublish("",QUEUE_NAME,null,message.getBytes());
            System.out.println("消息发送完毕" + message);
        }

    }
}
