package com.hancy.rabbitmq.one;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.nio.charset.StandardCharsets;

/**
 * @author Tomorrow
 * @create 2021-12-13 23:18
 * 生产者 发消息
 */
public class Producer {
    //队列名称
    public static final String QUEUE_NAME = "hello";

    //发消息
    public static void main(String[] args) throws Exception {
        //创建一个连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        //工厂IP连接RabbitMQ队列
        factory.setHost("172.0.0.1");
        //用户名
        factory.setUsername("guest");
        //密码
        factory.setPassword("guest");

        //创建链接
        Connection connection = factory.newConnection();
        //获取信道
        Channel channel = connection.createChannel();

        /**
         * 产生一个队列
         * 1、队列名称
         * 2、队列中消息是否持久化，默认在内存中
         * 3、该队列是否只供一个消费者进行消费，是否进行消费共享 true可以多个消费者消费
         * 4、是否自动删除，最后一个消费者断开连接后是否自动删除
         * 5、其他参数
         */
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);
        //发消息
        String message = "hello world";

        /**
         * 发送一个参数
         * 1、发送那个交换机
         * 2、路由的key
         * 3、其他参数信息
         * 4、发送消息的消息体
         */
        channel.basicPublish("",QUEUE_NAME,null,message.getBytes());

        System.out.println("消息发送完毕");

    }
}
