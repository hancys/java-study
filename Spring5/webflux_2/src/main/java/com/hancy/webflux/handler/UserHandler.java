package com.hancy.webflux.handler;

import com.hancy.webflux.entity.User;
import com.hancy.webflux.service.UserService;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.springframework.web.reactive.function.BodyInserters.fromObject;

/**
 * 函数编程
 * @author Tomorrow
 * @create 2021-08-28 20:54
 */
public class UserHandler {

    private final UserService userService;
    public UserHandler(UserService userService){
        this.userService = userService;
    }

    public Mono<ServerResponse> getUserById(ServerRequest request){
        //获取id
        int id = Integer.valueOf(request.pathVariable("id"));
        //空值处理
        Mono<ServerResponse> notFound = ServerResponse.notFound().build();
        //调用方法
        Mono<User> userMono = this.userService.gerUserById(id);
        //把 userMono 进行转换返回
        //使用Reactor操作符flatMap
        return userMono.flatMap(person ->
                ServerResponse.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(fromObject(person))
                        .switchIfEmpty(notFound)
        );
    }

    public Mono<ServerResponse> getAllUser(ServerRequest request){
        //调用方法得到结果
        Flux<User> users = this.userService.getAllUser();
        return ServerResponse.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(users,User.class);
    }

    public Mono<ServerResponse> saveUser(ServerRequest request){

        //得到user对象
        Mono<User> userMono = request.bodyToMono(User.class);
        return ServerResponse.ok()
                .build(this.userService.saveUserInfo(userMono));
    }

}
