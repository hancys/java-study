package com.hancy.webflux.service.impl;

import com.hancy.webflux.entity.User;
import com.hancy.webflux.service.UserService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Tomorrow
 * @create 2021-08-28 20:17
 */
@Service
public class UserServiceImpl implements UserService {

    private final Map<Integer,User> users = new HashMap<>();
    public UserServiceImpl(){
        this.users.put(1,new User("lucy","男",10));
        this.users.put(2,new User("bob","女",20));
        this.users.put(3,new User("jack","女",30));

    }

    @Override
    public Mono<User> gerUserById(Integer id) {
        return Mono.justOrEmpty(this.users.get(id));
    }

    @Override
    public Flux<User> getAllUser() {
        return Flux.fromIterable(this.users.values());
    }

    @Override
    public Mono<Void> saveUserInfo(Mono<User> userMono) {
        return userMono.doOnNext(person -> {
            //向map中放值
            int size = users.size() + 1;
            users.put(size,person);
        }).thenEmpty(Mono.empty());
    }
}
