package com.han.spring.bean;

/**
 * @author Tomorrow
 * @create 2021-08-11 22:31
 */
public class Book {
    private Integer id;
    private String zhanghao;
    private String mima;
    private Integer leixing;

    public Book() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getZhanghao() {
        return zhanghao;
    }

    public void setZhanghao(String zhanghao) {
        this.zhanghao = zhanghao;
    }

    public String getMima() {
        return mima;
    }

    public void setMima(String mima) {
        this.mima = mima;
    }

    public Integer getLeixing() {
        return leixing;
    }

    public void setLeixing(Integer leixing) {
        this.leixing = leixing;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", zhanghao='" + zhanghao + '\'' +
                ", mima='" + mima + '\'' +
                ", leixing='" + leixing + '\'' +
                '}';
    }
}
