package com.han.spring.dao;

import com.han.spring.bean.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Tomorrow
 * @create 2021-08-11 22:28
 */
@Repository
public class BookDaoImpl implements BookDao{
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void add(Book book) {
        String sql = "insert into denglu value (?,?,?,?)";
        Object[] args = {book.getId(), book.getZhanghao(), book.getMima(), book.getLeixing()};
        int update = jdbcTemplate.update(sql, args);
        System.out.println(update);
    }

    @Override
    public void update(Book book) {
        String sql = "update denglu set zhanghao=?,mima=?,leixing=? where id=?";
        Object[] args = {book.getZhanghao(), book.getMima(), book.getLeixing(),book.getId()};
        int update = jdbcTemplate.update(sql, args);
        System.out.println(update);
    }

    @Override
    public void delete(Integer id) {
        String sql = "delete from denglu where id=?";
        int update = jdbcTemplate.update(sql, id);
        System.out.println(update);
    }

    @Override
    public int selectCount() {
        String sql = "select count(*) from denglu";
        Integer num = jdbcTemplate.queryForObject(sql, Integer.class);
        return num;
    }

    @Override
    public Book findBook(int id) {
        String sql = "select * from denglu where id = ?";
        Book book = jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<Book>(Book.class),id);
        return book;
    }

    @Override
    public List<Book> findAll() {
        String sql = "select * from denglu";
        List<Book> query = jdbcTemplate.query(sql, new BeanPropertyRowMapper<Book>(Book.class));
        return query;
    }

    @Override
    public void batchAddBook(List<Object[]> bat) {
        String sql = "insert into denglu value (?,?,?,?)";
        int[] update = jdbcTemplate.batchUpdate(sql,bat);
        System.out.println(update);
    }

    @Override
    public void batchDelete(List<Object[]> bat) {
        String sql = "delete from denglu where id=?";
        int[] delete = jdbcTemplate.batchUpdate(sql,bat);
    }

    @Override
    public void batchUpdate(List<Object[]> bat) {
        String sql = "update denglu set zhanghao=?,mima=?,leixing=? where id=?";
        int[] delete = jdbcTemplate.batchUpdate(sql,bat);
    }
}
