package com.han.spring.dao;

import com.han.spring.bean.Book;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Tomorrow
 * @create 2021-08-11 22:26
 */

public interface BookDao {
    void add(Book book);
    void update(Book book);
    void delete(Integer id);
    int selectCount();

    Book findBook(int id);

    List<Book> findAll();

    void batchAddBook(List<Object[]> bat);

    void batchDelete(List<Object[]> bat);

    void batchUpdate(List<Object[]> bat);
}
