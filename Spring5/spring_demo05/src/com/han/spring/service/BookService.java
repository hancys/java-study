package com.han.spring.service;

import com.han.spring.bean.Book;
import com.han.spring.dao.BookDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Tomorrow
 * @create 2021-08-11 22:27
 */
@Service
public class BookService {

    @Autowired
    private BookDao bookDao;

    public void addBook(Book book) {
        bookDao.add(book);
    }

    public void updateBook(Book book) {
        bookDao.update(book);
    }

    public void deleteBook(Integer id) {
        bookDao.delete(id);
    }

    public int selectCount() {
        return bookDao.selectCount();
    }

    public Book findBook(int id) {
        return bookDao.findBook(id);
    }

    public List<Book> findAllBook() {
        return bookDao.findAll();
    }

    public void batchAdd(List<Object[]> bat) {
        bookDao.batchAddBook(bat);
    }

    public void batchDelete(List<Object[]> bat){
        bookDao.batchDelete(bat);
    }

    public void batchUpdate(List<Object[]> bat){
        bookDao.batchUpdate(bat);
    }
}
