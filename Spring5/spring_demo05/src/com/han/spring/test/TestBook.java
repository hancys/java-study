package com.han.spring.test;


import com.han.spring.bean.Book;
import com.han.spring.service.BookService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tomorrow
 * @create 2021-08-11 22:48
 */
public class TestBook {
    @Test
    public void test1(){
        ApplicationContext context =
                new ClassPathXmlApplicationContext("bean1.xml");
        BookService bookService = context.getBean("bookService", BookService.class);
        Book book = new Book();
        book.setZhanghao("hancy");
        book.setMima("123456");
        book.setLeixing(1);
        bookService.addBook(book);
    }

    @Test
    public void test2(){
        ApplicationContext context =
                new ClassPathXmlApplicationContext("bean1.xml");
        BookService bookService = context.getBean("bookService", BookService.class);
        Book book = new Book();
        book.setId(6);
        book.setZhanghao("hancy123");
        book.setMima("123456");
        book.setLeixing(0);
        bookService.updateBook(book);

        bookService.deleteBook(5);
    }

    @Test
    public void test3(){
        ApplicationContext context =
                new ClassPathXmlApplicationContext("bean1.xml");
        BookService bookService = context.getBean("bookService", BookService.class);

        System.out.println(bookService.selectCount());
        System.out.println(bookService.findBook(1));
        System.out.println(bookService.findAllBook());
    }

    @Test
    public void test4(){
        ApplicationContext context =
                new ClassPathXmlApplicationContext("bean1.xml");
        BookService bookService = context.getBean("bookService", BookService.class);

        List<Object[]> objects = new ArrayList<>();
        Object[] o1 = {null,"123","123456",1};
        Object[] o2 = {null,"abc","1234567",1};
        Object[] o3 = {null,"qwer","1234568",0};
        objects.add(o1);
        objects.add(o2);
        objects.add(o3);
        bookService.batchAdd(objects);
    }

    @Test
    public void test5(){
        ApplicationContext context =
                new ClassPathXmlApplicationContext("bean1.xml");
        BookService bookService = context.getBean("bookService", BookService.class);

        List<Object[]> objects = new ArrayList<>();
        Object[] o1 = {"123","123456",0,3};
        Object[] o2 = {"abc","1234567",1,4};
        Object[] o3 = {"qwer","1234568",0,5};
        objects.add(o1);
        objects.add(o2);
        objects.add(o3);
        bookService.batchUpdate(objects);
        List<Object[]> objects2 = new ArrayList<>();
        Object[] l1 = {5};
        Object[] l2 = {4};
        objects2.add(l1);
        objects2.add(l2);
        bookService.batchDelete(objects2);

    }
}
