package com.han.spring.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author Tomorrow
 * @create 2021-08-11 21:56
 */
@Configuration
@ComponentScan(value = "com.han.spring")
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class ConfigAop {
}
