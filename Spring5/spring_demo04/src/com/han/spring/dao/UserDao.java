package com.han.spring.dao;

/**
 * @author Tomorrow
 * @create 2021-08-10 23:12
 */
public interface UserDao {
    int add(int a,int b);
    String update(String id);
}
