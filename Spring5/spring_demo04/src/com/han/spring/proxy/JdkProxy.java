package com.han.spring.proxy;

import com.han.spring.dao.UserDao;
import com.han.spring.dao.UserDaoImpl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;

/**
 * @author Tomorrow
 * @create 2021-08-10 23:14
 */
public class JdkProxy {
    public static void main(String[] args) {
        Class[] interfaces = {UserDao.class};
        /*Proxy.newProxyInstance(JdkProxy.class.getClassLoader(), interfaces, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                return null;
            }
        });*/

        UserDao o = (UserDao) Proxy.newProxyInstance(JdkProxy.class.getClassLoader(), interfaces, new UserDaoProxy(new UserDaoImpl()));
        int add = o.add(1, 2);
        System.out.println(add);

    }
}

class UserDaoProxy implements InvocationHandler{
    //通过有参构造传对象
    private Object obj;
    public UserDaoProxy(Object obj){
        this.obj = obj;
    }


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        System.out.println("方法前....." + method.getName() + Arrays.toString(args));

        Object res = method.invoke(obj, args);

        System.out.println("方法后........" + obj);

        return res;
    }
}
