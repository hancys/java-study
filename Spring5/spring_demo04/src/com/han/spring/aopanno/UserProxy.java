package com.han.spring.aopanno;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author Tomorrow
 * @create 2021-08-11 20:33
 */
@Aspect
@Component
@Order(1) //值越小 优先级越高
public class UserProxy {

    @Pointcut(value = "execution(* com.han.spring.aopanno.User.*(..))")
    public void demo(){

    }


    @Before(value = "demo()")
    public void before(){
        System.out.println("前置通知...");
    }

    @AfterReturning(value = "execution(* com.han.spring.aopanno.User.*(..))")
    public void afterReturn(){
        System.out.println("后置通知...");
    }

    @Around(value = "execution(* com.han.spring.aopanno.User.*(..))")
    public void around(ProceedingJoinPoint p) throws Throwable {
        System.out.println("环绕前通知...");
        p.proceed();
        System.out.println("环绕后通知...");
    }

    @AfterThrowing(value = "execution(* com.han.spring.aopanno.User.*(..))")
    public void throwing(){
        System.out.println("异常通知...");
    }

    @After(value = "execution(* com.han.spring.aopanno.User.*(..))")
    public void after(){
        System.out.println("最终通知...");
    }
}
