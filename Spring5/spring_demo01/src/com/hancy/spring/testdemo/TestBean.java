package com.hancy.spring.testdemo;

import com.hancy.spring.Book;
import com.hancy.spring.bean.Emp;
import com.hancy.spring.service.UserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Tomorrow
 * @create 2021-08-09 22:11
 */
public class TestBean {
    @Test
    public void  testBook(){
        //1、加载spring配置文件
        ApplicationContext context = new ClassPathXmlApplicationContext("bean2.xml");


        //2、获取创建的对象
        UserService userService = context.getBean("userService", UserService.class);

        userService.add();


    }

    @Test
    public void  testBook2(){
        //1、加载spring配置文件
        ApplicationContext context = new ClassPathXmlApplicationContext("bean3.xml");


        //2、获取创建的对象
        Emp emp = context.getBean("emp", Emp.class);

        emp.add();


    }
}
