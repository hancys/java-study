package com.hancy.spring.testdemo;

import com.hancy.spring.Book;
import com.hancy.spring.Orders;
import com.hancy.spring.User;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Tomorrow
 * @create 2021-08-09 19:56
 */
public class TestSpring {

    @Test
    public void  testAdd(){
        //1、加载spring配置文件
        ApplicationContext context = new ClassPathXmlApplicationContext("bean1.xml");


        //2、获取创建的对象
        User user = context.getBean("user", User.class);

        System.out.println(user);

        user.add();
    }

    @Test
    public void  testBook(){
        //1、加载spring配置文件
        ApplicationContext context = new ClassPathXmlApplicationContext("bean1.xml");


        //2、获取创建的对象
        Book book = context.getBean("book", Book.class);

        System.out.println(book);

        book.toString();
    }

    @Test
    public void  testOrders(){
        //1、加载spring配置文件
        ApplicationContext context = new ClassPathXmlApplicationContext("bean1.xml");


        //2、获取创建的对象
        Orders order = context.getBean("orders", Orders.class);

        System.out.println(order);

        order.toString();
    }
}
