package com.hancy.spring.bean;

/**
 * @author Tomorrow
 * @create 2021-08-09 22:16
 */
public class Dept {
    private String dname;

    public void setDname(String dname) {
        this.dname = dname;
    }

    @Override
    public String toString() {
        return "Dept{" +
                "dname='" + dname + '\'' +
                '}';
    }
}
