package com.hancy.spring.bean;

/**
 * @author Tomorrow
 * @create 2021-08-09 22:17
 */
public class Emp {
    private String ename;
    private String gender;
    private Dept dept;

    public void add(){
        System.out.println(this.ename + "::" + this.gender+this.dept);
    }

    public Dept getDept() {
        return dept;
    }

    public void setDept(Dept dept) {
        this.dept = dept;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
