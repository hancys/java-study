package com.hancy.spring;

/**
 * @author Tomorrow
 * @create 2021-08-09 21:31
 */
public class Orders {
    private String oname;
    private String address;


    public Orders(String oname, String address) {
        this.oname = oname;
        this.address = address;
    }

    @Override
    public String toString() {
        return "Orders{" +
                "oname='" + oname + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
