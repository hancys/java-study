package com.han.spring.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Tomorrow
 * @create 2021-08-10 22:39
 */

@Configuration
@ComponentScan(value = {"com.han.spring"})
public class SpringConfig {
}
