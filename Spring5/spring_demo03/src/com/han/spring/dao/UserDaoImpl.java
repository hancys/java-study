package com.han.spring.dao;

import org.springframework.stereotype.Repository;

/**
 * @author Tomorrow
 * @create 2021-08-10 22:21
 */

@Repository(value = "userDaoName")
public class UserDaoImpl implements UserDao{

    @Override
    public void add() {
        System.out.println("add.......");
    }
}
