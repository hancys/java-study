package com.han.spring.testdemo;

import com.han.spring.config.SpringConfig;
import com.han.spring.service.UserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Tomorrow
 * @create 2021-08-10 21:49
 */
public class TestDemo {
    @Test
    public void test1(){
        ApplicationContext context =
                new ClassPathXmlApplicationContext("bean1.xml");
        UserService user = context.getBean("userService", UserService.class);
        System.out.println(user);
        user.add();
    }
    @Test
    public void test2(){
        ApplicationContext context =
                new AnnotationConfigApplicationContext(SpringConfig.class);
        UserService user = context.getBean("userService", UserService.class);
        System.out.println(user);
        user.add();
    }

}
