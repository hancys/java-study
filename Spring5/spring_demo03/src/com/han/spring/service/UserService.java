package com.han.spring.service;

import com.han.spring.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author Tomorrow
 * @create 2021-08-10 21:50
 */

@Service
public class UserService {

    @Autowired
    @Qualifier(value = "userDaoName")
    //@Resource(name = "") 根据类型 使用默认 根据名称加上name
    private UserDao userDao;

    @Value(value = "abc")
    private String name;
    public void add(){
        System.out.println("add......." + name);
        userDao.add();
    }
}
