package com.han.spring.service;

import com.han.spring.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Tomorrow
 * @create 2021-08-14 19:58
 */

@Service
@Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.REPEATABLE_READ,timeout = -1,readOnly = false)
public class UserService {

    @Autowired
    private UserDao userDao;

    public void money(){
 //       try {

            userDao.reduceMoney();
         //   int i = 1/0;
            userDao.addMoney();
/*        }catch (Exception e){

            e.printStackTrace();
        }*/

    }
}
