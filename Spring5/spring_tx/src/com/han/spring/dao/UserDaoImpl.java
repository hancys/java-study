package com.han.spring.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * @author Tomorrow
 * @create 2021-08-14 19:59
 */
@Repository
public class UserDaoImpl implements UserDao{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void addMoney() {
        String sql = "update t_count set money = money + ? where name = ?";
        jdbcTemplate.update(sql,100,"tangc");
    }

    @Override
    public void reduceMoney() {
        String sql = "update t_count set money = money - ? where name = ?";
        jdbcTemplate.update(sql,100,"hancy");

    }
}
