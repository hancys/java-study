package com.han.spring.dao;

/**
 * @author Tomorrow
 * @create 2021-08-14 19:58
 */
public interface UserDao {

    void addMoney();
    void reduceMoney();
}
