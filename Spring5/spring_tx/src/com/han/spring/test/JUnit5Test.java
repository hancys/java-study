package com.han.spring.test;

import com.han.spring.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Tomorrow
 * @create 2021-08-14 22:32
 */
//@ExtendWith(SpringExtension.class)
//@ContextConfiguration("classpath:bean2.xml")

@SpringJUnitConfig(locations = "classpath:bean2.xml")
public class JUnit5Test {

    @Autowired
    //@Qualifier("UserService")
    private UserService userService;

    @Test
    public void test(){
        userService.money();
    }
}
