package com.han.spring.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Tomorrow
 * @create 2021-08-14 22:15
 */
public class User {
    private static final Logger log = LoggerFactory.getLogger(User.class);

    public static void main(String[] args) {
        log.info("hello!");
    }
}
