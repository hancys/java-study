package com.han.spring.test;

import com.han.spring.config.TxConfig;
import com.han.spring.service.UserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

/**
 * @author Tomorrow
 * @create 2021-08-14 20:16
 */
public class testCount {

    @Test
    public void test(){
        ApplicationContext context =
                new ClassPathXmlApplicationContext("bean1.xml");
        UserService userService = context.getBean("userService", UserService.class);
        userService.money();
    }

    @Test
    public void test2(){
        ApplicationContext context =
                new ClassPathXmlApplicationContext("bean2.xml");
        UserService userService = context.getBean("userService", UserService.class);
        userService.money();
    }

    @Test
    public void test3(){
        ApplicationContext context =
                new AnnotationConfigApplicationContext(TxConfig.class);
        UserService userService = context.getBean("userService", UserService.class);
        userService.money();
    }

    @Test
    public void test4(){
        GenericApplicationContext context =
                new GenericApplicationContext();
        context.refresh();
        context.registerBean("user",User.class,()->new User());
        context.getBean("user");
    }

}
