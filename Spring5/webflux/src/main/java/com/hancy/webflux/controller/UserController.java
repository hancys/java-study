package com.hancy.webflux.controller;

import com.hancy.webflux.entity.User;
import com.hancy.webflux.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author Tomorrow
 * @create 2021-08-28 20:25
 */
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/user/{id}")
    public Mono<User> getUserById(@PathVariable Integer id){
        return userService.gerUserById(id);
    }

    @GetMapping("/user")
    public Flux<User> getAllUser(){
        return userService.getAllUser();
    }
    @PostMapping("/saveUser")
    public Mono<Void> saveUser(@RequestBody User user){
        Mono<User> userMono = Mono.just(user);
        return userService.saveUserInfo(userMono);
    }
}
