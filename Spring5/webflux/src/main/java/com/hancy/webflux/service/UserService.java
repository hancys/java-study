package com.hancy.webflux.service;

import com.hancy.webflux.entity.User;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author Tomorrow
 * @create 2021-08-28 20:14
 */
public interface UserService {
    //根据id查询
    Mono<User> gerUserById(Integer id);
    //查询所有
    Flux<User> getAllUser();
    //添加用户
    Mono<Void> saveUserInfo(Mono<User> user);
}
