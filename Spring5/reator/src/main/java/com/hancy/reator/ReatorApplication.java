package com.hancy.reator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReatorApplication.class, args);
    }

}
