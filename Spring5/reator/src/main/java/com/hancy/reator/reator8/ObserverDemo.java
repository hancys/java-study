package com.hancy.reator.reator8;

import java.util.Observable;

/**
 * @author Tomorrow
 * @create 2021-08-26 21:34
 */
public class ObserverDemo extends Observable {
    public static void main(String[] args) {

        Observable observer= new ObserverDemo();
        //添加观察者
        observer.addObserver((o,arg)->{
            System.out.println("发生了变化");
        });

        observer.addObserver((o,arg)->{
            System.out.println("收到被观察的通知");
        });
        observer.notifyObservers();
    }
}
