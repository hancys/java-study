package com.hancy.mvc.controller;

import com.hancy.mvc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Tomorrow
 * @create 2021-08-22 16:49
 */
//@Controller
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("/query")
    public void querry(){
        userService.page();
    }
}
