package com.hancy.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Tomorrow
 * @create 2021-08-16 22:19
 */
@Controller
public class JspController {
    @RequestMapping("/success")
    public String success(){

        return "success";
    }
}
