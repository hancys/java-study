package com.hancy.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Tomorrow
 * @create 2021-08-15 16:30
 */

@Controller
public class TestController {

    @RequestMapping("/")
    public String index(){
        return "index";
    }

    @RequestMapping("/params")
    public String params(){
        return "test_params";
    }

}
