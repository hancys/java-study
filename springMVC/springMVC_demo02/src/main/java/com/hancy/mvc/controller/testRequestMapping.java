package com.hancy.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Tomorrow
 * @create 2021-08-15 16:49
 */
@Controller
//@RequestMapping("/hello")
public class testRequestMapping {

    @RequestMapping(
            value = {"/testRequestMapping","/test"},
            method = {RequestMethod.GET,RequestMethod.POST}
    )
    public String success(){
        return "success";
    }

    @RequestMapping(
            value = {"/testParams"},
            params = {"username"}
    )
    public String testParams(){
        return "success";
    }

    @RequestMapping(
            value = {"/q*q/testAnt"}
    )
    public String testAnt(){
        return "success";
    }
    @RequestMapping(
            value = {"/testRest/{id}"}
    )
    public String testRest(@PathVariable("id") Integer id ){
        System.out.println(id);
        return "success";
    }
}
