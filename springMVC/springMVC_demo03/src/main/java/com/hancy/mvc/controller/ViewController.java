package com.hancy.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Tomorrow
 * @create 2021-08-16 21:13
 */

@Controller
public class ViewController {
    @RequestMapping("/testView")
    public String testView(){
        return "success";
    }

    @RequestMapping("/testForward")
    public String testForward(){
        return "forward:/testView";
    }

    @RequestMapping("/testRedirect")
    public String testRedirect(){
        return "redirect:/testView";
    }
}
