package com.hancy.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Tomorrow
 * @create 2021-08-16 10:26
 */
@Controller
public class TestController {

    /*@RequestMapping("/")
    public String index(){
        return "index";
    }*/

    @RequestMapping("/test")
    public String test(){
        return "test_view";
    }
}
