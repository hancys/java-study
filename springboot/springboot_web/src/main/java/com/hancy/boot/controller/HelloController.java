package com.hancy.boot.controller;

import org.springframework.web.bind.annotation.*;

/**
 * @author Tomorrow
 * @create 2021-08-23 21:39
 */
@RestController
public class HelloController {

    @RequestMapping("/bug.jpg")
    public String hello(){
        return "bug.jpg";
    }

    //    @RequestMapping(value = "/user",method = RequestMethod.GET)
    @GetMapping("/user")
    public String getUser(){

        return "GET-张三";
    }

    //    @RequestMapping(value = "/user",method = RequestMethod.POST)
    @PostMapping("/user")
    public String saveUser(){
        return "POST-张三";
    }


    //    @RequestMapping(value = "/user",method = RequestMethod.PUT)
    @PutMapping("/user")
    public String putUser(){

        return "PUT-张三";
    }

    @DeleteMapping("/user")
//    @RequestMapping(value = "/user",method = RequestMethod.DELETE)
    public String deleteUser(){
        return "DELETE-张三";
    }

}
