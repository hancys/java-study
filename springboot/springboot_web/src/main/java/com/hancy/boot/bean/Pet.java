package com.hancy.boot.bean;

import lombok.Data;

@Data
public class Pet {

    private String name;
    private Integer age;

}
