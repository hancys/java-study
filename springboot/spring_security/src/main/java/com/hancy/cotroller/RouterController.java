package com.hancy.cotroller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Tomorrow
 * @create 2021-08-24 20:50
 */
@Controller
public class RouterController {

    @RequestMapping({"/","/index"})
    public String index(){
        return "index";
    }

    @RequestMapping("/toLogin")
    public String toLogin(){
        return "views/login";
    }

    @RequestMapping("/level1/{id}")
    public String level1(@PathVariable("id") int id){
        return "views/level1/"+id;
    }

    @RequestMapping("/level2/{id}")
    //执行验证权限后执行方法
    @PreAuthorize("hasAnyAuthority('admins')")
    //先执行方法后验证
    @PostAuthorize("hasAnyAuthority('admins')")
    public String level2(@PathVariable("id") int id){
        return "views/level2/"+id;
    }

    @RequestMapping("/level3/{id}")
    //权限验证
    @Secured({"ROLE_sale","ROLE_manager"})
    //返回结果做过滤
    @PostFilter("filterObject.username == 'admin'")
    //传入的参数做过滤
    @PreFilter(value = "filterObject.id%2==0")
    public String level3(@PathVariable("id") int id){
        return "views/level3/"+id;
    }



}
