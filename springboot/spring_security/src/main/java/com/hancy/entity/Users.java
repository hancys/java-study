package com.hancy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author Tomorrow
 * @create 2021-08-27 10:46
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Users {
    private Integer id;
    private String zhanghao;
    private String mima;
    private String leixing;
}
