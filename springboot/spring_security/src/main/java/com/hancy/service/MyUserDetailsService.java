package com.hancy.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hancy.entity.Users;
import com.hancy.mapper.UsersMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Tomorrow
 * @create 2021-08-27 10:26
 */
@Service("userDetailsService")
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UsersMapper usersMapper;

    @Override
    public UserDetails loadUserByUsername(String zhanghao) throws UsernameNotFoundException {

        //调用方法 根据用户名
        QueryWrapper<Users> wrapper = new QueryWrapper<>();
        wrapper.eq("zhanghao",zhanghao);
        Users users = usersMapper.selectOne(wrapper);
        if (users == null){
            throw new UsernameNotFoundException("用户名不存在");
        }


        List<GrantedAuthority> auths = AuthorityUtils.commaSeparatedStringToAuthorityList("role");
        return new User(users.getZhanghao(),new BCryptPasswordEncoder().encode(users.getMima()),auths);
    }
}
