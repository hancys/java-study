package com.hancy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hancy.entity.Users;
import org.springframework.stereotype.Repository;

/**
 * @author Tomorrow
 * @create 2021-08-27 10:48
 */
@Repository
public interface UsersMapper extends BaseMapper<Users> {
}
