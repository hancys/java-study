package com.hancy;

import lombok.AllArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

@SpringBootTest
class SpringbootTestApplicationTests {

    @Autowired
    JavaMailSenderImpl mailSender;

    @Test
    void contextLoads() {

        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();

        simpleMailMessage.setSubject("你好！");
        simpleMailMessage.setText("谢谢！");
        simpleMailMessage.setTo("hansel2021@163.com");
        simpleMailMessage.setFrom("hansel2021@163.com");
        mailSender.send(simpleMailMessage);
    }

    @Test
    void contextLoads2() throws MessagingException {

        MimeMessage mimeMessage = mailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

        helper.setSubject("han");
        helper.setText("<p style='color:red'>谢谢<p>",true);
        helper.addAttachment("1.jpg",new File("C:\\Users\\TomORrow\\Desktop\\1.jpg"));
        helper.setTo("hansel2021@163.com");
        helper.setFrom("hansel2021@163.com");

        mailSender.send(mimeMessage);
    }

}
