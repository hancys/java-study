package com.hancy.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * @author Tomorrow
 * @create 2021-08-25 16:16
 */

@Service
public class ScheduledService {

    @Scheduled(cron = "")
    public void hello(){
        System.out.println("执行了");
    }

}
