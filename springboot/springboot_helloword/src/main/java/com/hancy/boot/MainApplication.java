package com.hancy.boot;

import ch.qos.logback.core.db.DBHelper;
import com.hancy.boot.bean.Pet;
import com.hancy.boot.bean.User;
import com.hancy.boot.config.MyConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author Tomorrow
 * @create 2021-08-23 16:36
 */

/**
 * 主程序类
 * @SpringBootApplication 这是一个springboot应用
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})

/*@SpringBootConfiguration
@EnableAutoConfiguration
@ComponentScan*/
public class MainApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(MainApplication.class, args);

        String[] names = run.getBeanDefinitionNames();
        for (String name : names) {
            System.out.println(name);
        }

        //从容器中获取组件 默认为单实例
        //run.getBean("pet01", Pet.class);

        MyConfig bean = run.getBean(MyConfig.class);
        System.out.println(bean);

        //代里对象调用方法 @Configuration(proxyBeanMethods = true)
        //springboot总会检查 这个组件是否在容器中有 保持组件单实例
        /*User user = bean.user01();
        User user1 = bean.user01();
        System.out.println(user==user1);*/

        /*User user01 = run.getBean("user01", User.class);
        Pet cat = run.getBean("cat", Pet.class);
        System.out.println(user01.getPet() == cat);*/

        String[] beanNamesForType = run.getBeanNamesForType(User.class);
        for (String s : beanNamesForType) {
            System.out.println(s);
        }

        DBHelper bean1 = run.getBean(DBHelper.class);
        System.out.println(bean1);
    }
}
