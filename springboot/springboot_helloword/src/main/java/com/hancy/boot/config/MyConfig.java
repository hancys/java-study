package com.hancy.boot.config;

import ch.qos.logback.core.db.DBHelper;
import com.hancy.boot.bean.Car;
import com.hancy.boot.bean.Pet;
import com.hancy.boot.bean.User;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

/**
 * @author Tomorrow
 * @create 2021-08-23 18:34
 */
/**
 * 配置类中使用@Bean 默认也是单实例的
 * 配置类本身也是一个组件
    proxyBeanMethods 代理的方法
  full(proxyBeanMethods = true)
  lite(proxyBeanMethods = false)
  组件依赖
  @Import({User.class, DBHelper.class}) 指定组件放入容器中 默认组件的名字是全类名
 */
@Import({User.class, DBHelper.class})
@Configuration(proxyBeanMethods = true)
//@ImportResource("classpath:bean.xml") //导入配置文件
//开启car的属性配置绑定功能 把指定的组件自动注入到容器中
@EnableConfigurationProperties(Car.class)
public class MyConfig {

    /**
     * 外部无论对配置类的组件待用方法 都是之前注册到容器的单实例
     * @return
     */
    @ConditionalOnBean(name = "cat")
    @Bean
    public User user01(){
        User han = new User("han", 18);
        //user组件依赖的pet组件
        han.setPet(pet01());
        return han;
    }
    @Bean("cat")
    public Pet pet01(){
        return new Pet("cat",99.3);
    }
}
