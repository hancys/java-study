package com.hancy.boot.bean;

import lombok.*;

/**
 * @author Tomorrow
 * @create 2021-08-23 18:33
 */
@Data
@ToString
@NoArgsConstructor
//@AllArgsConstructor
@EqualsAndHashCode
public class User {
    private String name;
    private int age;
    private Pet pet;

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
