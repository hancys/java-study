package com.hancy.boot.bean;

import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Tomorrow
 * @create 2021-08-23 19:18
 */

/**
 * 只有在容器中的组件 才可以绑定
 */
@Data
@ToString
@Component
@ConfigurationProperties(prefix = "mycar")
public class Car {
    private String brand;
    private Integer price;
}
