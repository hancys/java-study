package com.hancy.boot.controller;

import com.hancy.boot.bean.Car;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Tomorrow
 * @create 2021-08-23 16:39
 */
/*
@Controller
@ResponseBody
*/

@Slf4j
@RestController
public class HelloController {

    @Autowired
    Car car;

    @RequestMapping("/hello")
    public String handle01(){
        log.info("请求进来了");
        return "Hello SpringBoot";
    }

    @RequestMapping("/car")
    public Car car(){
        return car;
    }
}
