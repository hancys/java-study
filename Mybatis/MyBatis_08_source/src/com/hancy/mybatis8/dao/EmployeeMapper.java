package com.hancy.mybatis8.dao;

import com.hancy.mybatis8.bean.Employee;

public interface EmployeeMapper {
	
	public Employee getEmpById(Integer id);

}
