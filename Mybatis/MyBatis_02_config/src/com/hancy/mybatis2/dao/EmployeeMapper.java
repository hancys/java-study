package com.hancy.mybatis2.dao;

import com.hancy.mybatis2.bean.Employee;

public interface EmployeeMapper {
	
	public Employee getEmpById(Integer id);

}
