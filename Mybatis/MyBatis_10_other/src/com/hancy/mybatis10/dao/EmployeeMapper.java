package com.hancy.mybatis10.dao;

import java.util.List;

import com.hancy.mybatis10.bean.Employee;
import com.hancy.mybatis10.bean.OraclePage;

public interface EmployeeMapper {
	
	public Employee getEmpById(Integer id);
	
	public List<Employee> getEmps();
	
	public Long addEmp(Employee employee);
	
	public void getPageByProcedure(OraclePage page);
}
