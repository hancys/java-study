package com.hancy.mybatis1.dao;

import com.hancy.mybatis1.bean.Employee;

public interface EmployeeMapper {
	
	public Employee getEmpById(Integer id);

}
