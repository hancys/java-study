package com.hancy.mybatis9.dao;

import com.hancy.mybatis9.bean.Employee;

public interface EmployeeMapper {
	
	public Employee getEmpById(Integer id);

}
