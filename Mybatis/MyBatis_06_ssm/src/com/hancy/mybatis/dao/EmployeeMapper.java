package com.hancy.mybatis.dao;


import java.util.List;

import com.hancy.mybatis.bean.Employee;

public interface EmployeeMapper {
	
	public Employee getEmpById(Integer id);
	
	public List<Employee> getEmps();
	

}
