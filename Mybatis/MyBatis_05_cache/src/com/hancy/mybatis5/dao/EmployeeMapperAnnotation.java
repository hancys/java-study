package com.hancy.mybatis5.dao;

import org.apache.ibatis.annotations.Select;

import com.hancy.mybatis5.bean.Employee;

public interface EmployeeMapperAnnotation {
	
	@Select("select * from tbl_employee where id=#{id}")
	public Employee getEmpById(Integer id);
}
