package com.hancy.mp.mapper5;

import com.hancy.mp.beans.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;

public interface UserMapper  extends BaseMapper<User>{

}
