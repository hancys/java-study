package com.hancy.mp.service;

import com.hancy.mp.beans.Employee;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author weiyunhui
 * @since 2018-06-21
 */
public interface EmployeeService extends IService<Employee> {

}
