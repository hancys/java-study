package com.han.domain;

public interface Equipment {
	
	String getDescription();
}
