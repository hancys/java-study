package com.han.java01;

/*
变量的使用
1. java定义变量的格式：数据类型 变量名 = 变量值;

2. 说明：
   ① 变量必须先声明，后使用
   ② 变量都定义在其作用域内。在作用域内，它是有效的。换句话说，出了作用域，就失效了
   ③ 同一个作用域内，不可以声明两个同名的变量

*/
class VariableTest {
	public static void main(String[] args) {
		//�����Ķ���
		int myAge = 12;
		//������ʹ��
		System.out.println(myAge);
		
		//�������ʹ��myNumber֮ǰ��δ�����myNumber
		//System.out.println(myNumber);

		//����������
		int myNumber;
		
		//�������ʹ��myNumber֮ǰ��δ��ֵ��myNumber
		//System.out.println(myNumber);

		//�����ĸ�ֵ
		myNumber = 1001;
		//���벻ͨ��
		//System.out.println(myClass);

		//��������ͬһ���������ڶ���ͬ���ı���
		//int myAge = 22;
		
	}

	public void method(){
		int myClass = 1;
	}
}

//class VariableTest {}//����˼ά����֤��