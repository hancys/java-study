package com.han.exer;

import org.junit.Test;

import java.io.*;

/**
 * @author Tomorrow
 * @create 2021-08-05 15:55
 */
public class FileCopy{

    @Test
    public void copy(){
        File in;
        FileReader src = null;
        File out ;
        FileWriter dest = null;
        try {
            in = new File("F:\\JavaSE学习\\JavaSenior\\file_IO\\hello.txt");

            src = new FileReader(in);

            char[] buff = new char[5];
            int len;
            int i = 1;
            while(-1 != (len = src.read(buff))){
                out = new File("hello_copy"+ i +".txt");
                dest= new FileWriter(out);
                dest.write(buff,0,len);
                i++;
                if(dest != null){
                    dest.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (src != null){
                try {
                    src.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Test
    public void copy2() {
        FileReader fr = null;
        FileWriter fw = null;
        try {
            File destFile = new File("hello123copy.txt");
            fw = new FileWriter(destFile,true);
            for (int i = 1; i <= 5; i++) {
                //1.创建File类的对象，指明读入和写出的文件
                File srcFile = new File("F:\\JavaSE学习\\JavaSenior\\hello_copy" + i + ".txt");
                //2.创建输入流和输出流的对象
                fr = new FileReader(srcFile);
                //3.数据的读入和写出操作
                char[] cbuf = new char[5];
                int len;//记录每次读入到cbuf数组中的字符的个数
                while ((len = fr.read(cbuf)) != -1) {
                    //每次写出len个字符
                    fw.write(cbuf, 0, len);

                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //4.关闭流资源
            //方式一：
            /*try {
                if(fw != null){
                    fw.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }finally{
                try {
                    if(fr != null){
                        fr.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }*/
            //方式二：
            try {
                if (fw != null) {
                    fw.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                if (fr != null) {
                    fr.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

}
