package com.han.serializable;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 */
public class FileUtilsTest {

    public static void main(String[] args) {
        File srcFile = new File("a\\爱情与友情.jpg");
        File destFile = new File("a\\爱情与友情2.jpg");

        try {
            FileUtils.copyFile(srcFile,destFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
