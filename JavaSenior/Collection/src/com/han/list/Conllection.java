package com.han.list;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Collection接口中声明的方法的测试
 *
 * 结论：
 * 向Collection接口中的实现类对象中添加obj对象时，要求obj所在类要重写equals()方法
 *
 * @author Tomorrow
 * @create 2021-07-25 16:11
 */
public class Conllection {

    @Test
    public void test1(){

        Collection coll = new ArrayList();
        coll.add(123);
        coll.add(456);
        coll.add(new String("Tom"));
        coll.add(false);
        //Person p = new Person("Jeery",20);
        //coll.add(p);
        coll.add(new Person("Jeery",20));
        //contains(Object obj) 判断当前集合中是否包含obj对象
        //我们在判断时回调用obj对象所在类的equals方法
        boolean contains = coll.contains(123);
        System.out.println(contains);//true
        System.out.println(coll.contains(new String("Tom")));//true
        //System.out.println(coll.contains(p));//true
        System.out.println(coll.contains(new Person("Jeery",20)));//false

        ////containsAll(collection coll1) 判断形参中的所有元素 是否都存在于当前的集合中
        Collection coll1 = Arrays.asList(123,456);
        coll.containsAll(coll1);
    }

    @Test
    public void test2(){
        //3.remove(Object obj) 从当前的集合中移除obj元素
        Collection coll = new ArrayList();
        coll.add(123);
        coll.add(456);
        coll.add(new String("Tom"));
        coll.add(false);
        coll.add(new Person("Jeery",20));

        coll.remove(1234);
        System.out.println(coll);

        coll.remove(new Person("Jeery",20));
        System.out.println(coll);

        //4、removeAll(Collection coll1) 从当前集合中移除coll1中的所有元素 （差集）
        Collection coll1 = Arrays.asList(123,4567);
        coll.removeAll(coll1);
        System.out.println(coll);

    }

    @Test
    public void test3(){
        Collection coll = new ArrayList();
        coll.add(123);
        coll.add(456);
        coll.add(new String("Tom"));
        coll.add(false);
        coll.add(new Person("Jeery",20));

        //5.retainAll(Collection coll1) 获取交集，并返回给当前集合（修改）
        Collection coll1 = Arrays.asList(123,456,789);
        coll.retainAll(coll1);
        System.out.println(coll);

        //6.equals(Object obj) 判断当前集合和形参集合是否都相同
        Collection coll2 = new ArrayList();
        coll2.add(123);
        coll2.add(456);
        /*coll2.add(new String("Tom"));
        coll2.add(false);
        coll2.add(new Person("Jeery",20));*/

        System.out.println(coll.equals(coll2));
    }

    @Test
    public void test4(){
        Collection coll = new ArrayList();
        coll.add(123);
        coll.add(456);
        coll.add(new String("Tom"));
        coll.add(false);
        coll.add(new Person("Jeery",20));
        //hashCode() 返回当前对象的哈希值
        System.out.println(coll.hashCode());

        //集合转化为数组 toArray()
        Object[] arr = coll.toArray();
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }

        //数组转化为集合  Array.asList() 可变形参 调用Arrays类的静态方法
        List<String> list = Arrays.asList(new String[]{"AA", "BB", "CC"});
        System.out.println(list);

        List<int[]> ints = Arrays.asList(new int[]{123, 456});//会把一个数组当作一个对象（错误）
        List<Integer> integers = Arrays.asList(new Integer[]{123, 456});//正确
        List ints1 = Arrays.asList(123, 456);
        System.out.println(ints);
        System.out.println(ints1);
        System.out.println(integers);

        //iterator(): 返回Iterator接口的实例，用于遍历集合元素，放在IteratorTest.java测试
    }
}
