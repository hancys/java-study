package com.han.list;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * 集合元素的遍历 使用Iterator接口 迭代器接口
 * 内部的方法 hasNext() 判断是否有下一个元素
 *        和 next() 指针下移，将指针上的位置的元素返回
 *每次调用iterator 都会生成一个全新的迭代器对象
 * remove() 可以在遍历的时候，删除集合中的元素 不同于集合中的remove()
 *      在未调用next()之前 或者 调用两次remove() 会报错  IllegalStateException
 *      （必须在next之后调用一次）
 *
 * @author Tomorrow
 * @create 2021-07-25 17:03
 */
public class IteratorTest {
    @Test
    public void test1(){
        Collection coll = new ArrayList();
        coll.add(123);
        coll.add(456);
        coll.add(new String("Tom"));
        coll.add(false);
        coll.add(new Person("Jeery",20));

        Iterator iterator = coll.iterator();
        //方式一
        /*System.out.println(iterator.next());
        System.out.println(iterator.next());
        System.out.println(iterator.next());
        System.out.println(iterator.next());
        System.out.println(iterator.next());*/

        //报异常.NoSuchElementException
        //System.out.println(iterator.next());

        //方式二 不推荐
        for (int i = 0; i < coll.size(); i++) {
            System.out.println(iterator.next());
        }

        //方式三 推荐
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }

    @Test
    public void test2(){
        Collection coll = new ArrayList();
        coll.add(123);
        coll.add(456);
        coll.add(new String("Tom"));
        coll.add(false);
        coll.add(new Person("Jeery",20));

        Iterator iterator = coll.iterator();

        //错误
        /*while (iterator.next() != null){
            System.out.println(iterator.next());
        }*/

        //死循环 输出3  每次调用iterator 都会生成一个全新的迭代器对象
        /*while (coll.iterator().hasNext()){
            System.out.println(coll.iterator().next());
        }*/

    }

    @Test
    public void test3(){
        Collection coll = new ArrayList();
        coll.add(123);
        coll.add(456);
        coll.add(new String("Tom"));
        coll.add(false);
        coll.add(new Person("Jeery",20));

        Iterator iterator = coll.iterator();
//        iterator.remove(); 报错
        while (iterator.hasNext()){
            Object obj = iterator.next();
            if("Tom".equals(obj)){
                iterator.remove();
                // iterator.remove(); 报错 需要再next之后
            }
        }

        Iterator iterator1 = coll.iterator();
        while (iterator1.hasNext()){
            System.out.println(iterator1.next());
        }

    }
}
