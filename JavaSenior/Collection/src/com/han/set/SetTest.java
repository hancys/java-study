package com.han.set;

import com.han.list.Person;
import org.junit.Test;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * 1.Set接口的框架
 * |----Collection接口：单列集合，用来存储一个一个的对象
 *           |----Set接口：存储无序的、不可重复的数据   -->高中讲的“集合”
 *               |----HashSet 作为Set接口的主要实现类 线程不安全，可以存储null值
 *                      |----LinkedHashSet 作为HashSet的子类，遍历其内部数据时，可以按照添加的顺序去遍历
 *                         对于频繁的遍历操作，LinkedHashSet效率高
 *               |----TreeSet 使用二叉树（红黑树存储），同一个类的new的，按照添加对象的某些指定属性排序
 *                              对象排序接口：Comparable Comparator
 * 底层也是数组，+链表 初始容量时16，如果使用率超过0.75，0.75*16 = 12；就会扩容到原来的2倍 依次是32 64.....
 * 当一个位置形成的链表的结点个数达到8并且table的长度大于64 那么就会进行黑红树的转换
 * 1.set接口中没有定义额外定义的方法
 * 2.向Set中添加的数组，其所在的类一定要重写equals()和hashCode()方法
 *     重写的两个方法尽可能保持一致性，相等的对象具有相同的散列码
 *     重写方法时，两个方法中使用的属性尽量一致
 *
 * @author Tomorrow
 * @create 2021-07-26 16:52
 */
public class SetTest {
    /*
        一、Set接口：存储无序的、不可重复的数据
        以HashSet为例
        1.无序性:不等于随机性.存储的数据在底层数组中并非按照数组索引的顺序添加，是根据数组的哈希值存储（数组长度16）

        2.不可重复性 : 保证添加的元素，按照equals()判断时，不能返回true，即 相同的元素不能添加，只能有一个

        二、添加元素的过程：以HashSet为例
            我们向HashSet中添加元素a，首先调用元素a所在类的hashCode()方法，计算元素a的哈希值
            此哈希值接着通过某种算法计算出再HashSet底层数组的存放位置（索引位置），判断数组此位置上已经有元素
                如果此位置上没有其他元素，则添加成功
                如果此位置上的其他元素b或者以链表方式存在的多个元素，则比较元素a与b的hash值
                    如果hash值不一样则不相同，则添加成功    元素a与已经存在指定索引位置上以链表的方式存储
                    如果hash值相同，进而需要调用元素a的equals()方法
                        equals方法返回true，则添加失败
                        equals方法返回false，则添加成功
           jdk7 元素a放到数组中，指向原来的元素
           jdk8 在数组中原来的元素 指向元素a
     */

    @Test
    public void test1(){
        Set set = new HashSet();
        set.add(456);
        set.add(123);
        set.add("AA");
        set.add("CC");
        set.add(new User("Tom",12));
        set.add(new User("Tom",12));
        set.add(129);

        Iterator iterator = set.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }
    /*
    * LinkedHashSet的使用
    * LinkedHashSet作为HashSet的子类，在添加数据的同时，每个数据还维护了两个引用，记录此数据的前一个数据和后一个数据
    * 优点：对于频繁的遍历操作，LinkedHashSet效率高
    * */


    @Test
    public void test2(){
        Set set = new LinkedHashSet();
        set.add(456);
        set.add(123);
        set.add("AA");
        set.add("CC");
        set.add(new User("Tom",12));
        set.add(new User("Tom",12));
        set.add(129);

        Iterator iterator = set.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }
}
