package com.han.set;

import org.junit.Test;

import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * @author Tomorrow
 * @create 2021-07-26 19:13
 */
public class TreeSetTest {
     /*1、向TreeSet当中添加的数据，要求是相同类的对象，不能添加不同类的对象
       set.add(456);
       set.add(123);
       set.add("AA");
       set.add("CC");
       set.add(new User("Tom",12));
       报错

       2.两种排序方式：自然排序（实现Comparable接口）和定制排序

       3、自然排序中，比较两个对象是否相同的标准，compareTo()返回0，不再是equals().
          红黑树：有序的二叉树 左小右大
       4、在定制排序中 比较两个对象是否相同的标准，compare()返回0，不再是equals().
*/

    @Test
    public void test1(){
        TreeSet set = new TreeSet();

        //举例一
        /*set.add(456);
        set.add(123);
        set.add(-450);
        set.add(789);*/


        set.add(new User("Tom",12));
        set.add(new User("Jery",15));
        set.add(new User("Jim",25));
        set.add(new User("Mike",5));


        Iterator iterator = set.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }

    @Test
    public void test2(){
        Comparator com = new Comparator() {
            //按照年龄 从小到大排列
            @Override
            public int compare(Object o1, Object o2) {
                if(o1 instanceof User && o2 instanceof User){
                    User u1 = (User) o1;
                    User u2 = (User) o2;
                    return Integer.compare(u1.getAge(), u2.getAge());
                }else {
                    throw new RuntimeException("类型不匹配");
                }
            }
        };
        TreeSet set = new TreeSet(com);

        //举例一
        /*set.add(456);
        set.add(123);
        set.add(-450);
        set.add(789);*/


        set.add(new User("Tom",12));
        set.add(new User("Jery",15));
        set.add(new User("Jim",25));
        set.add(new User("Mike",5));


        Iterator iterator = set.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }
}
