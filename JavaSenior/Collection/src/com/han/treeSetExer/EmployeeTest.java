package com.han.treeSetExer;

import org.junit.Test;

import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * 创建该类的 5 个对象，并把这些对象放入 TreeSet 集合中（下一章：TreeSet 需使用泛型来定义）
 *  分别按以下两种方式对集合中的元素进行排序，并遍历输出：
 *
 *  1). 使Employee 实现 Comparable 接口，并按 name 排序
 *  2). 创建 TreeSet 时传入 Comparator对象，按生日日期的先后排序。
 *
 * @author Tomorrow
 * @create 2021-07-26 19:50
 */
public class EmployeeTest {
    //问题一 ： 使用自然排序
    @Test
    public void test1(){
        TreeSet set = new TreeSet();

        Employee e1 = new Employee("Job", 55, new MyDate(1999, 5, 4));
        Employee e2 =new Employee("Mary",33,new MyDate(1995,5,8));
        Employee e3 =new Employee("Bob",22,new MyDate(1996,8,4));
        Employee e4 =new Employee("Mike",44,new MyDate(1999,9,4));
        Employee e5 =new Employee("Chenlong",66,new MyDate(1999,5,9));

        set.add(e1);
        set.add(e2);
        set.add(e3);
        set.add(e4);
        set.add(e5);

        Iterator iterator = set.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }

    //问题二：用生日排序
    @Test
    public void test2() {

        TreeSet set = new TreeSet(new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                if(o1 instanceof Employee && o2 instanceof Employee){
                    Employee e1 = (Employee) o1;
                    Employee e2 = (Employee) o2;

                    MyDate b1 = e1.getBrithday();
                    MyDate b2 = e2.getBrithday();
                    //方式一
                    /*//比较年
                    int minusYear = b1.getYear() - b2.getYear();
                    if (minusYear != 0){
                        return minusYear;
                    }
                    //比较月
                    int minusMonth = b1.getMonth() - b2.getMonth();
                    if (minusMonth != 0){
                        return minusMonth;
                    }
                    //比较日
                    int minusDay = b1.getDay() - b2.getDay();
                    if (minusDay != 0){
                        return minusDay;
                    }*/

                    //方式二
                    return b1.compareTo(b2);
                }
                return 0;
            }
        });

        Employee e1 = new Employee("Job", 55, new MyDate(1999, 5, 4));
        Employee e2 =new Employee("Mary",33,new MyDate(1995,5,8));
        Employee e3 =new Employee("Bob",22,new MyDate(1996,8,4));
        Employee e4 =new Employee("Mike",44,new MyDate(1999,9,4));
        Employee e5 =new Employee("Chenlong",66,new MyDate(1999,5,9));

        set.add(e1);
        set.add(e2);
        set.add(e3);
        set.add(e4);
        set.add(e5);

        Iterator iterator = set.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }
}
