package com.han.java;

/**
 * @author Tomorrow
 * @create 2021-07-21 20:43
 */
public class Person {

    String name;
    int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Person() {
    }
}
