package com.han.java1;

import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;

/**
 * @author Tomorrow
 * @create 2021-07-21 20:43
 */
@Inherited
@Repeatable(MyAnnotations.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({TYPE, FIELD, METHOD, PARAMETER, CONSTRUCTOR, LOCAL_VARIABLE,TYPE_PARAMETER,TYPE_USE})
public @interface MyAnnotation {

    String value() default "hello";
}
