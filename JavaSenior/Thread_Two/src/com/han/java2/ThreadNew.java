package com.han.java2;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * 创建线程的方式三，实现Callable接口  JDK5.0
 *
 * 如何理解实现Callable接口的方式要比实现Runnable接口创建多线程更强大
 * 1.call有返回值
 * 2.call可以抛出异常，被外面的操作不获，获取异常信息
 * 3.Callable接口 支持泛型
 *
 * @author Tomorrow
 * @create 2021-07-23 20:42
 */
//1.创建一个Callable接口的实现类
class NumThread implements Callable{
    //2.实现call方法，将此线程需要执行的操作声明在call中
    @Override
    public Object call() throws Exception {
        int sum = 0;
        for (int i = 1; i <= 100; i++) {
            if (i % 2 == 0){
                System.out.println(i);
                sum += i;
            }
        }
        return sum;
    }
}

public class ThreadNew {
    public static void main(String[] args) {
        //3.创建Callable接口实现类对象
        NumThread numThread = new NumThread();
        //4.将此Callable接口实现类的对象传递到FutureTask的构造器中，创建FutureTask对象
        FutureTask futureTask = new FutureTask(numThread);
        //5.将FutureTask的对象作为参数传递到Thread类的构造器中，创建Thread对象，并调用start()方法
        Thread thread = new Thread(futureTask);
        thread.start();

        try {
            //6.获取Callable方法中的返回值
            //get()方法的返回值，即为futureTask构造器参数Callable实现重写call()的返回值
            Object sum = futureTask.get();
            System.out.println(sum);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
