package com.han.java2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 创建线程的方式4：线程池
 *
 *  好处：
 *  1.提高响应速度（减少了创建新线程的时间）
 *  2.降低资源消耗（重复利用线程池中线程，不需要每次都创建）
 *  3.便于线程管理
 *       corePoolSize：核心池的大小
 *       maximumPoolSize：最大线程数
 *       keepAliveTime：线程没有任务时最多保持多长时间后会终止
 *
 *
 *  面试题：创建多线程有几种方式？四种！
 *
 * @author Tomorrow
 * @create 2021-07-23 21:12
 */
class NumberThread implements Runnable{

    @Override
    public void run() {
        for (int i = 1; i <=100 ; i++) {
            if (i % 2 == 0){
                System.out.println(i);
            }
        }
    }
}
class NumberThread1 implements Runnable{

    @Override
    public void run() {
        for (int i = 1; i <=100 ; i++) {
            if (i % 2 != 0){
                System.out.println(i);
            }
        }
    }
}

public class ThreadPool {
    public static void main(String[] args) {
        //1.提供指定线程数量的线程池
        ThreadPoolExecutor service = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
        service.setCorePoolSize(15);
        //service.setKeepAliveTime();
        //设置线程池的属性


        //2.执行指定的线程的操作，需要提供实现Runnable接口或者Callable接口实现类的对象
        //适合Runnable
        service.execute(new NumberThread());
        service.execute(new NumberThread1());
        //适合适用于Callable
        //service.submit(Callable callable);

        //关闭线程池
        service.shutdown();
    }
}
