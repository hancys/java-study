package com.han.java2;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 线程通信的例子，使用两个线程打印 1--100 ， 线程1，线程2，交替打印
 * 涉及的三个方法
 * wait() 一旦执行此方法，当前线程就进入阻塞状态，并释放同步监视器
 * notify() 一旦执行此方法，就会唤醒wait的一个线程，如果有多个线程被wait，就唤醒优先级高的线程
 * notifyAll() 一旦执行此方法，就会唤醒所有被wait的线程
 *
 * @author Tomorrow
 * @create 2021-07-23 19:41
 */
class Number2 implements Runnable{
    private int number = 1;
    private ReentrantLock lock = new ReentrantLock();
    private Condition condition = lock.newCondition();
    @Override
    public void run() {
        while (true){
            try {
                lock.lock();
                condition.signalAll();
                if (number <= 100){
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName() + number);
                    number++;
                    condition.await();
                }else {
                    break;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
    }
}
public class CommunicationTest2 {
    public static void main(String[] args) {
        Number2 number2 = new Number2();
        Thread thread = new Thread(number2);
        Thread thread2 = new Thread(number2);

        thread.setName("甲");
        thread2.setName("乙");

        thread.start();
        thread2.start();
    }
}
