package com.han.java2;

/**
 * 线程通信的例子，使用两个线程打印 1--100 ， 线程1，线程2，交替打印
 * 涉及的三个方法
 * wait() 一旦执行此方法，当前线程就进入阻塞状态，并释放同步监视器
 * notify() 一旦执行此方法，就会唤醒wait的一个线程，如果有多个线程被wait，就唤醒优先级高的线程
 * notifyAll() 一旦执行此方法，就会唤醒所有被wait的线程
 *
 * 说明：
 * 1.wait()/notify()/notifyAll() 三个方法必须使用在同步代码块或者同步方法中
 * 2.调用方法的调用者 必须是同步代码块或同步方法中的同步监视器，
 *   否则会出现异常（IllegalMonitorStateException 非法监控异常）
 * 3.定义在java.lang.Object
 *   调用时任何一个对象充当同步监视器 都必须要有此三个方法
 *
 *   sleep() 和 wait() 异同
 *   1.相同点：一旦执行方法，都可以使得当前的线程进入阻塞状态
 *   2.不同点：1)两个方法声明的位置不同：Thread类中声明sleep(),Object类中声明wait()
 *            2)调用的范围要求不同，sleep()可以在任何需要的场景下调用，wait()必须在同步代码或者同步方法中。
 *            3)关于是否释放同步监视器：如果两个方法都是用在同步代码块或同步方法中，sleep不会释放锁，wait会释放锁
 *
 * @author Tomorrow
 * @create 2021-07-23 19:41
 */
class Number implements Runnable{
    private int number = 1;
    @Override
    public void run() {
        while (true){
            synchronized (this) {
                notify();
                if (number <= 100){
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName() + number);
                    number++;
                    try {
                        //使得调用如下wait方法的线程进入阻塞状态
                        //wait会释放锁。
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }else {
                    break;
                }
            }
        }
    }
}
public class CommunicationTest {
    public static void main(String[] args) {
        Number number = new Number();
        Thread thread = new Thread(number);
        Thread thread2 = new Thread(number);

        thread.setName("甲");
        thread2.setName("乙");

        thread.start();
        thread2.start();
    }
}
