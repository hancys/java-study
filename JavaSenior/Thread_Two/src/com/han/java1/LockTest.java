package com.han.java1;

import java.util.concurrent.locks.ReentrantLock;

/**
 * 解决线程安全问题的方式三：Lock锁 JDK5.0
 *
 * 1.面试题 synchronized 与 lock 异同
 *  相同：二者都可以解决线程的安全问题
 *  不同：synchronized机制，在执行完相同的同步代码以后，自动的释放同步监控
 *       Lock 需要手动的启动同步lock() 手动结束同步unlock()
 *
 * 2.如何解决线程安全问题的几种方式
 * @author Tomorrow
 * @create 2021-07-23 17:11
 */

class Window implements Runnable{
    private int ticket = 100;
    //1.实例化ReentrantLock
    private ReentrantLock lock = new ReentrantLock();

    @Override
    public void run() {
        while (true){
            try {
                //2.调用lock方法
                lock.lock();

                if (ticket>0){
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName() + ticket);
                    ticket--;
                }else {
                    break;
                }
            }finally {
                //3.调用解锁方法 unlock()
                lock.unlock();
            }
        }
    }
}

public class LockTest {
    public static void main(String[] args) {
        Window window = new Window();

        Thread thread = new Thread(window);
        Thread thread2 = new Thread(window);
        Thread thread3 = new Thread(window);

        thread.setName("线程一");
        thread2.setName("线程二");
        thread3.setName("线程三");

        thread.start();
        thread2.start();
        thread3.start();

    }
}
