package com.han.java1;

/**
 * 使用同步机制将单例模式中的懒汉模式改写为线程安全
 *
 * @author Tomorrow
 * @create 2021-07-23 16:24
 */
public class BankTest {
}

class Bank{
    private Bank(){}
    private static Bank instance = null;
    public static /*synchronized*/ Bank getInstance(){
       //同步方法 锁为类本身
        // 方式一 效率差
        /*synchronized (Bank.class) {
            if(instance == null){
                instance = new Bank();
            }
            return instance;
        }*/
        //方式二 效率比方式一更好一些
        if(instance == null){
            synchronized (Bank.class) {
                if(instance == null){
                    instance = new Bank();
                }
            }
        }
        return instance;
    }
}
