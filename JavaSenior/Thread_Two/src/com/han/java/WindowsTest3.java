package com.han.java;

/**
 * 使用同步方法解决实现Runnable接口的线程安全问题
 *
 * 关于同步方法的总结：
 * 1.同步方法仍然涉及同步监视器，只是不需要我们显示的声明
 * 2.非静态的同步方法，同步监视器：this
 *   静态的同步方法，同步监视器：此类
 *
 * @author Tomorrow
 * @create 2021-07-23 16:04
 */
class Windows3 implements Runnable{
    private int ticket = 100;
    @Override
    public void run() {
        while (true){
            show();
        }
    }
    //同步监视器 this
    private synchronized void show(){
        if (ticket > 0) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("当前票是：" + ticket);
            ticket--;
        }
    }
}

public class WindowsTest3 {
    public static void main(String[] args) {
        //实例化一个对象，只用一个类成员
        Windows3 w = new Windows3();
        Thread thread = new Thread(w);
        Thread thread2 = new Thread(w);
        Thread thread3 = new Thread(w);

        thread.setName("窗口一");
        thread2.setName("窗口二");
        thread3.setName("窗口三");

        thread.start();
        thread2.start();
        thread3.start();

    }
}