package com.han.java;

/**
 * 创建三个窗口卖票，总票数100张
 * 存在线程安全问题
 * 实现Runnable接口的类
 *
 * 1、问题，买票过程中，出现了重票和错票的问题。 出现了线程安全问题
 * 2、问题出现的原因，当某个线程操作车票的过程中，尚未操作完成时，其他线程参与进来，也操作了车票
 * 如何解决：当一个线程，在操作共享数据时，其他线程不能参与进来，直到当前线程操作结束结束，其他线程才可以操作
 * 及时当前线程为阻塞，也不能进入操作
 * 4、在Java中通过同步机制，解决线程安全问题
 * 方式一：同步代码块
 * synchronized(同步监视器){
 *      //需要同步的代码
 * }
 * 说明：操作共享数据的代码，就是同步的代码  -->不能包含代码多，也不能包含代码少
 *      共享数据：多个线程共同操作的数据（变量）
 *      同步监视器，俗称：锁  任何一个类的对象，都可以充当锁（可以使用当前对象 this）
 *      要求：多个线程共用同一个锁
 *      补充：实现Runnable接口方式创建线程的方式中，可以考虑使用this（表示当前对象）充当同步监视器
 *              在继承Thread类创建线程的方式中，慎用this充当锁，可以考虑用类充当。
 * 方式二：同步方法
 *      如果操作共享数据的代码完整的声明在一个方法中，我们可以将此方法声明同步
 * 5、同步的方式：解决了线程的安全问题，优势
 *      操作代码时，只能有一个线程参与，其他线程等待相当于单线程的过程效率低。局限性
 *
 * @author Tomorrow
 * @create 2021-07-22 20:00
 */

class Windows1 implements Runnable{
    private int ticket = 100;
    Object obj = new Object();
    @Override
    public void run() {
        while (true){
            //此时的this唯一的Windows对象
             synchronized(this){
             //synchronized(obj) {
                if (ticket > 0) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("当前票是：" + ticket);
                    ticket--;
                } else {
                    break;
                }
            }
        }
    }
}

public class WidowsTest1 {
    public static void main(String[] args) {
        //实例化一个对象，只用一个类成员
        Windows1 windows1 = new Windows1();
        Thread thread = new Thread(windows1);
        Thread thread2 = new Thread(windows1);
        Thread thread3 = new Thread(windows1);

        thread.setName("窗口一");
        thread2.setName("窗口二");
        thread3.setName("窗口三");

        thread.start();
        thread2.start();
        thread3.start();

    }
}
