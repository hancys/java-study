package com.han.java;

/**
 * 使用同步方法处理继承Thread类处理数据
 *
 * @author Tomorrow
 * @create 2021-07-23 16:13
 */

class Windows4 extends Thread{
    //所有线程都共享一个静态资源
    private static int ticket = 100;

    @Override
    public void run() {

        while (true){
            show();
        }
    }
    private static synchronized void show(){//静态方法 同步监视器：此类
    //此时还是为不安全的 new三个对象，存在三个锁 必须使用静态
    //private synchronized void show(){
        if (ticket > 0) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + ": 买票的票号为 ：" + ticket);
            ticket--;
        }
    }
}


public class WindowsTest4 {
    public static void main(String[] args) {
        Windows4 t1 = new Windows4();
        Windows4 t2 = new Windows4();
        Windows4 t3 = new Windows4();

        t1.setName("窗口一");
        t2.setName("窗口二");
        t3.setName("窗口三");

        t1.start();
        t2.start();
        t3.start();
    }
}