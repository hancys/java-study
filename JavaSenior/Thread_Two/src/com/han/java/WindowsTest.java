package com.han.java;

/**
 * 创建三个窗口卖票，总票数100张
 * 使用同步代码块 解决继承Thread类的方式线程安全问题
 *
 * @author Tomorrow
 * @create 2021-07-22 19:28
 */

class Windows extends Thread{
    //所有线程都共享一个静态资源
    private static int ticket = 100;
    //每个线程都需要同一个锁，new三个对象 存在三个锁 需要static修饰，共享数据
    static Object obj = new Object();
    @Override
    public void run() {

        while (true){
            //类也是对象  Class class = Windows.class 类只会加载一次，反射
            synchronized (Windows.class){
            //synchronized(obj) {
            //错误的方式 this代表当前对象 有new三个对象
            //synchronized (this){
                if (ticket > 0) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(getName() + ": 买票的票号为 ：" + ticket);
                    ticket--;
                } else {
                    break;
                }
            }
        }

    }
}

public class WindowsTest {
    public static void main(String[] args) {
        Windows t1 = new Windows();
        Windows t2 = new Windows();
        Windows t3 = new Windows();

        t1.setName("窗口一");
        t2.setName("窗口二");
        t3.setName("窗口三");

        t1.start();
        t2.start();
        t3.start();
    }
}
