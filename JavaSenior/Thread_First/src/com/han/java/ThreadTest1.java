package com.han.java;

/**
 * 创建多线程的方式二：
 * 实现Runnable接口
 * 1、创建一个实现了Runnable接口的类
 * 2、实现类去实现Runnable中的抽象方法
 * 3、创建实现类的对象
 * 4、将此对象传入到Thread类的构造器中，创建Thread类的对象
 * 5、通过Thread类的对象调用start()
 *
 * @author Tomorrow
 * @create 2021-07-22 19:42
 */
//1、创建一个实现Runnable接口的类
class MThrread implements Runnable{
    //2、实现类去实现Runnable中的抽象方法run()
    @Override
    public void run() {
        for (int i = 1; i < 100; i++) {
            if (i % 2 == 0){
                System.out.println(Thread.currentThread().getName() + i);
            }
        }
    }
}

public class ThreadTest1 {
    public static void main(String[] args) {
        //3、创建实现类的对象
        MyThread myThread = new MyThread();
        //4、将此对象传入到Thread类的构造器中，创建Thread类的对象
        //public Thread(Runnable target)
        Thread thread = new Thread(myThread);
        //修改线程名字，在启动之前
        thread.setName("线程一");
        //5、通过Thread类的对象调用start()
        // 启动线程 调用当前线程的run()-->调用了Runnable类型的target的run()方法
        thread.start();

        //再启动一个线程，遍历100以内的偶数
        Thread thread1 = new Thread(myThread);
        thread1.start();
    }
}
