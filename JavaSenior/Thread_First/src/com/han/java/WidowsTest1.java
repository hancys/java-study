package com.han.java;

/**
 * 创建三个窗口卖票，总票数100张
 * 存在线程安全问题
 * 实现Runnable接口的类
 *
 * @author Tomorrow
 * @create 2021-07-22 20:00
 */

class Windows1 implements Runnable{
    private int ticket = 100;
    @Override
    public void run() {
        while (true){
            if (ticket > 0){
                System.out.println("当前票是：" + ticket);
                ticket--;
            }else {
                break;
            }
        }
    }
}
public class WidowsTest1 {
    public static void main(String[] args) {
        //实例化一个对象，只用一个类成员
        Windows1 windows1 = new Windows1();
        Thread thread = new Thread(windows1);
        Thread thread2 = new Thread(windows1);
        Thread thread3 = new Thread(windows1);

        thread.setName("窗口一");
        thread2.setName("窗口二");
        thread3.setName("窗口三");

        thread.start();
        thread2.start();
        thread3.start();

    }
}
