package com.han.java;

/**
 * 多线程的创建  方式一：继承于Thread类
 * 1.创建一个继承于Thread类的子类
 * 2.重写Thread类中的run()方法 -->此线程执行的操作 声明在run()方法中
 * 3.创建Thread类的子类对象
 * 4.通过创建此对象调用start()
 *
 * @author Tomorrow
 * @create 2021-07-21 20:23
 */

//创建一个继承于Thread类的子类
class MyThread extends Thread{
    //重写Thread类中的run()方法
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            if(i % 2 == 0){
                System.out.println(Thread.currentThread().getName() + i);
            }
        }
    }
}
public class TheadTest {
    public static void main(String[] args) {
        //创建Thread类的子类对象
        MyThread t1 = new MyThread();
        //通过创建此对象调用start()
        //启动当前线程，调用当前线程的run()方法
        t1.start();
        // t1.run();不能直接调用run()方法 还在main中
        //t1.start(); java.lang.IllegalThreadStateException
        MyThread t2 = new MyThread();
        //多个线程需要多个对象
        t2.start();
        for (int i = 0; i < 100; i++) {
            if(i % 2 != 0){
                System.out.println(Thread.currentThread().getName()+ i + "******");
            }
        }
    }
}
