package com.han.java;

/**
 * 测试Thread中的常用方法
 * 1.start() 启动当前线程，调用当前线程的run()方法
 * 2.run() 通常需要重写Thread类中的此方法，将创建的线程要执行的操作声明在此方法中
 * 3.currentThread() : 静态方法 返回执行当前代码的线程
 * 4.getName():获取当前线程的名字
 * 5.setName() : 设置当前线程的名字
 * 6.yield() 释放当前cpu执行权
 * 7.join() 在线程A 中 调用线程B 的join()，此时线程A 为阻塞状态，知道线程B 结束
 * 8.stop() 强制结束线程，已过时。
 * 9.sleep(long millis) 让当前线程睡眠 指定的millitime 毫秒时间内
 * 10.isAlive()
 *
 * 线程的优先级：
 * 1.priority
 * MAX_PRIORITY : 10
 * MIN_PRIORITY : 1
 * NORM_PRIORITY : 5 默认
 * 2.如何获取和设置当前线程的优先级
 * getPriority():获取当前线程的优先级
 * setPriority():设置当前线程的优先级
 *
 * 说明：高优先级的线程要抢占低优先级的线程CPU的执行权，但是只是从概率上讲，
 * 高优先级的线程高概率的情况下被执行，并不意味着只有当高优先级的就一定被执行
 *
 * @author Tomorrow
 * @create 2021-07-21 21:01
 */

class HelloThread extends Thread{
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            if(i % 2 == 0){
                try {
                    sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + " " + Thread.currentThread().getPriority()+ i);
            }
            if(i % 20 == 0){
                yield();
            }
        }
    }
    public HelloThread(String name){
        super(name);
    }
}
public class ThreadMethodTest {
    public static void main(String[] args) {
        HelloThread helloThread = new HelloThread("Thread一");
        //helloThread.setName("线程一");

        //设置线程的优先级
        helloThread.setPriority(Thread.MAX_PRIORITY);
        helloThread.start();
        Thread.currentThread().setName("主线程");
        for (int i = 0; i < 100; i++) {
            if(i % 2 != 0){
                System.out.println(Thread.currentThread().getName() + i);
            }
            if(i == 20){
                try {
                    helloThread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println(helloThread.isAlive());
    }
}
