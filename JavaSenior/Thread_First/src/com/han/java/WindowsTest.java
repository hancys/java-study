package com.han.java;

/**
 * 创建三个窗口卖票，总票数100张
 * 存在线程安全问题
 *
 * @author Tomorrow
 * @create 2021-07-22 19:28
 */

class Windows extends Thread{
    //所有线程都共享一个静态资源
    private static int ticket = 100;

    @Override
    public void run() {

        while (true){
            if (ticket > 0){
                System.out.println(getName() + ": 买票的票号为 ：" + ticket);
                ticket--;
            }else{
                break;
            }
        }

    }
}

public class WindowsTest {
    public static void main(String[] args) {
        Windows t1 = new Windows();
        Windows t2 = new Windows();
        Windows t3 = new Windows();

        t1.setName("窗口一");
        t2.setName("窗口二");
        t3.setName("窗口三");

        t1.start();
        t2.start();
        t3.start();
    }
}
