package com.hancy.jedis;

import org.junit.Test;
import redis.clients.jedis.Jedis;

import java.util.List;
import java.util.Set;

/**
 * @author Tomorrow
 * @create 2021-08-25 21:56
 */
public class JedisDemo {

    public static void main(String[] args) {
        //创建jedis对象
        Jedis jedis = new Jedis("47.102.208.199",6379);
        String ping = jedis.ping();
        System.out.println(ping);
        jedis.close();
    }

    @Test
    public void demo(){
        Jedis jedis = new Jedis("47.102.208.199",6379);

        //jedis.set("name","lucy");

        String name = jedis.get("name");
        System.out.println(name);
        jedis.mset("k1","v1","k2","v2");
        List<String> mget = jedis.mget("k1", "k2");
        for (String s : mget) {
            System.out.println(s);
        }
        Set<String> keys = jedis.keys("*");
        for (String key : keys) {
            System.out.println(key);
        }
        jedis.close();
    }

    @Test
    public void demo2(){
        Jedis jedis = new Jedis("47.102.208.199",6379);

        jedis.lpush("key1","lucy","jack");
        List<String> key1 = jedis.lrange("key1", 0, -1);
        for (String s : key1) {
            System.out.println(s);
        }
        jedis.close();
    }

    @Test
    public void demo3(){
        Jedis jedis = new Jedis("47.102.208.199",6379);

        jedis.sadd("hancy","123","466");
        Set<String> hancy = jedis.smembers("hancy");
        System.out.println(hancy);
        jedis.close();
    }

    @Test
    public void demo4(){
        Jedis jedis = new Jedis("47.102.208.199",6379);

        jedis.hset("users","age","20");
        String hget = jedis.hget("users", "age");
        System.out.println(hget);
        jedis.close();
    }
    @Test
    public void demo5(){
        Jedis jedis = new Jedis("47.102.208.199",6379);

        jedis.zadd("china",100d,"shanghai");
        Set<String> china = jedis.zrange("china", 0, -1);
        System.out.println(china);
        jedis.close();
    }
}
