package com.hancy.jedis;

import redis.clients.jedis.Jedis;

import java.util.Random;

/**
 * @author Tomorrow
 * @create 2021-08-25 23:08
 */
public class PhoneCode {
    public static void main(String[] args) {
        //模拟验证码的发送
        verifyCode("15934436666");

        //getRedisCode("15934436666","896325");
    }

    //生成6位数字验证码
    public static String getCode(){
        Random random = new Random();
        String code="";
        for (int i = 0; i < 6; i++) {
            int rand = random.nextInt(10);
            code+=rand;
        }
        return code;
    }

    //让每个手机只能发送三次 设置过期时间
    public static void verifyCode(String phone){
        Jedis jedis = new Jedis("47.102.208.199",6379);

        //拼接手机次数
        String countKey="VerityCode"+phone+":count";
        //验证码
        String codeKey="VerityCode"+phone+":code";

        String s = jedis.get(countKey);
        if (s==null){
            //没有发送 第一次
            //设置次数
            jedis.setex(countKey,24*60*60,"1");
        }else if (Integer.parseInt(s) <= 2){
            //发送次数+1
            jedis.incr(countKey);
        }else if (Integer.parseInt(s) > 2){
            System.out.println("今天的发送次数已经达到三次了！");
            jedis.close();
        }

        //将验证码放到redis里
        String vcode = getCode();
        jedis.setex(codeKey,120,vcode);

    }

    //验证码的校验
    public static void getRedisCode(String phone,String code){
        //先从redis获取验证码
        Jedis jedis = new Jedis("47.102.208.199",6379);

        //验证码
        String codeKey="VerityCode"+phone+":code";

        String s = jedis.get(codeKey);
        //判断
        if(s.equals(code)){
            System.out.println("成功");
        }else {
            System.out.println("失败");
        }
        jedis.close();
    }

}
