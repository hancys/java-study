package countdownlatch;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 模拟100米跑步，等待裁判员发令
 * @author Tomorrow
 * @create 2021-09-06 14:21
 */
public class CountDownLatchDemo2 {
    public static void main(String[] args) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        CountDownLatch latch2 = new CountDownLatch(5);
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        for (int i = 0; i < 5; i++) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    System.out.println("等待发令");
                    try {
                        latch.await();
                        System.out.println("开始跑步了");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }finally {
                        latch2.countDown();
                    }
                }
            };
            executorService.submit(runnable);
        }
        Thread.sleep(5000);
        System.out.println("发令");
        latch.countDown();

        latch2.await();
        System.out.println("跑步完毕");
    }

}
