package threadlocal;

/**
 * 演示ThreadLocal避免重复传递参数的麻烦
 * @author Tomorrow
 * @create 2021-08-31 19:49
 */
public class ThreadLocalNormalMethod01 {

    public static void main(String[] args) {
        new Service().process();
    }

}
class Service{
    public void process(){
        User user = new User("hcy");
        UserContextHolder.holder.set(user);
        new Service2().process();
    }
}
class Service2{
    public void process(){
        User user = UserContextHolder.holder.get();
        System.out.println(user.name);
        new Service3().process();
    }
}
class Service3{
    public void process(){
        User user = UserContextHolder.holder.get();
        System.out.println(user.name);
    }
}
class UserContextHolder{
    public static ThreadLocal<User> holder = new ThreadLocal<>();
}
class User{
    String name;

    public User(String name) {
        this.name = name;
    }
}