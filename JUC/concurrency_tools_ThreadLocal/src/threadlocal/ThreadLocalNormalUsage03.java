package threadlocal;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 1000个线程同时运行 线程池
 * @author Tomorrow
 * @create 2021-08-31 19:11
 */
public class ThreadLocalNormalUsage03 {
    public static ExecutorService threalPool = Executors.newFixedThreadPool(10);
    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 1000; i++) {
            int finalI = i;
            threalPool.submit(new Runnable() {
                @Override
                public void run() {
                    String date = new ThreadLocalNormalUsage03().date(finalI);
                    System.out.println(date);
                }
            });
        }
        threalPool.shutdown();
    }

    public String date(int seconds){
        //参数的时间毫秒 1970年1月1日 00：00：00
        Date date = new Date(1000 * seconds);
        SimpleDateFormat simpleDateFormat =
                new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return simpleDateFormat.format(date);

    }
}
