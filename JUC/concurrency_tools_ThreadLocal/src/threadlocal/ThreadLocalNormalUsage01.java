package threadlocal;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 两个线程打印
 * @author Tomorrow
 * @create 2021-08-31 19:11
 */
public class ThreadLocalNormalUsage01 {
    public static void main(String[] args) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String date = new ThreadLocalNormalUsage01().date(10);
                System.out.println(date);
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                String date = new ThreadLocalNormalUsage01().date(1007);
                System.out.println(date);
            }
        }).start();
    }

    public String date(int seconds){
        //参数的时间毫秒 1970年1月1日 00：00：00
        Date date = new Date(1000 * seconds);
        SimpleDateFormat simpleDateFormat =
                new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return simpleDateFormat.format(date);

    }
}
