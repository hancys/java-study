package threadlocal;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 1000个线程同时运行 利用ThreadLocal给每个线程分配自己的dateFormat对象，保证了线程安全
 * 高效利用内存
 * @author Tomorrow
 * @create 2021-08-31 19:11
 */
public class ThreadLocalNormalUsage06 {
    public static ExecutorService threalPool = Executors.newFixedThreadPool(10);
    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 1000; i++) {
            int finalI = i;
            threalPool.submit(new Runnable() {
                @Override
                public void run() {
                    String date = new ThreadLocalNormalUsage06().date(finalI);
                    System.out.println(date);
                }
            });
        }
        threalPool.shutdown();
    }

    public String date(int seconds){
        //参数的时间毫秒 1970年1月1日 00：00：00
        Date date = new Date(1000 * seconds);
        /*SimpleDateFormat simpleDateFormat =
                new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");*/
        SimpleDateFormat simpleDateFormat = ThreadSafeFormat.dateFormatThreadLocal.get();
        return simpleDateFormat.format(date);

    }
}
class ThreadSafeFormat{
    public static ThreadLocal<SimpleDateFormat> dateFormatThreadLocal =
            new ThreadLocal<SimpleDateFormat>(){
                @Override
                protected SimpleDateFormat initialValue() {
                    return new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                }
    };


    public static ThreadLocal<SimpleDateFormat> dateFormatThreadLocal2 =
            ThreadLocal.withInitial(
                    ()-> new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
            );
}