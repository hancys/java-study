package threadlocal;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 30个线程同时运行
 * @author Tomorrow
 * @create 2021-08-31 19:11
 */
public class ThreadLocalNormalUsage02 {
    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 30; i++) {
            int finalI = i;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    String date = new ThreadLocalNormalUsage02().date(finalI);
                    System.out.println(date);
                }
            }).start();
            Thread.sleep(100);
        }
    }

    public String date(int seconds){
        //参数的时间毫秒 1970年1月1日 00：00：00
        Date date = new Date(1000 * seconds);
        SimpleDateFormat simpleDateFormat =
                new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return simpleDateFormat.format(date);

    }
}
