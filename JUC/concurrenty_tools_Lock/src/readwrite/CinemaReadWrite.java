package readwrite;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author Tomorrow
 * @create 2021-09-02 20:03
 */
public class CinemaReadWrite {
    private static ReentrantReadWriteLock reentrantReadWriteLock = new ReentrantReadWriteLock();
    private static ReentrantReadWriteLock.ReadLock readLock = reentrantReadWriteLock.readLock();
    private static ReentrantReadWriteLock.WriteLock writeLock = reentrantReadWriteLock.writeLock();
    private static void read(){
        readLock.lock();
        try {
            System.out.println("我得到了读锁");
        }finally {
            System.out.println("我释放了读锁");
            readLock.unlock();
        }
    }
    private static void write(){
        writeLock.lock();
        try {
            System.out.println("我得到了写锁");
        }finally {
            System.out.println("我释放了写锁");
            writeLock.unlock();
        }
    }

    public static void main(String[] args) {
        new Thread(()->read()).start();
        new Thread(()->read()).start();
        new Thread(()->write()).start();
        new Thread(()->write()).start();

    }
}
