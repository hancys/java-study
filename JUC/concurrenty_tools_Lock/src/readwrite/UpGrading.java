package readwrite;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 公平与不公平读写锁的策略
 * @author Tomorrow
 * @create 2021-09-02 20:32
 */
public class UpGrading {
    private static ReentrantReadWriteLock reentrantReadWriteLock = new ReentrantReadWriteLock(false);
    private static ReentrantReadWriteLock.ReadLock readLock = reentrantReadWriteLock.readLock();
    private static ReentrantReadWriteLock.WriteLock writeLock = reentrantReadWriteLock.writeLock();
    private static void read(){
        readLock.lock();
        try {
            System.out.println("我得到了读锁");
            Thread.sleep(20);
            writeLock.lock();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            writeLock.unlock();
            System.out.println("我释放了读锁");
            readLock.unlock();
        }
    }
    private static void write(){
        writeLock.lock();
        try {
            System.out.println("我得到了写锁");
            Thread.sleep(40);
            readLock.lock();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            System.out.println("我释放了写锁");
            writeLock.unlock();
        }
    }

    public static void main(String[] args) {
        new Thread(()->read()).start();
        new Thread(()->write()).start();
    }
}
