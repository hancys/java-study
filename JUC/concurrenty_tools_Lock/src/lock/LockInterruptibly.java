package lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Tomorrow
 * @create 2021-08-31 22:00
 */
public class LockInterruptibly implements Runnable{
    private Lock lock = new ReentrantLock();

    public static void main(String[] args) {
        LockInterruptibly lockInterruptibly = new LockInterruptibly();
        Thread thread = new Thread(lockInterruptibly);
        Thread thread1 = new Thread(lockInterruptibly);
        thread.start();
        thread1.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        thread.interrupt();

    }
    @Override
    public void run() {
        System.out.println("尝试获取锁");
        try {
            lock.lockInterruptibly();
            try {
                System.out.println("获取到了锁");
                Thread.sleep(5000);
            }catch (InterruptedException e){
                System.out.println("睡眠期间被中断");
            }finally{
                lock.unlock();
            }
        } catch (InterruptedException e) {
            System.out.println("等锁期间被中断");
        }
    }
}
