package lock;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Tomorrow
 * @create 2021-09-02 18:50
 */
public class PessimismOptimismLock {
    int a;
    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger();
        atomicInteger.decrementAndGet();
    }
    public synchronized void testMethod(){
        a++;
    }

}
