package lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 需要释放锁，异常时不会释放锁，所以finally里面释放锁，保证异常时也会释放锁
 * @author Tomorrow
 * @create 2021-08-31 21:32
 */
public class MustUnlock {
    private static Lock lock = new ReentrantLock();
    public static void main(String[] args) {
        lock.lock();
        try {
            //获取本锁保护的资源
        }finally {
            lock.unlock();
        }
    }
}
