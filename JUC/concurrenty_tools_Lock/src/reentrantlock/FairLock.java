package reentrantlock;

import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 演示公平与非公平两种情况
 * @author Tomorrow
 * @create 2021-09-02 19:37
 */
public class FairLock {
    public static void main(String[] args) throws InterruptedException {
        PrintQueue printQueue = new PrintQueue();
        Thread thread[] = new Thread[10];
        for (int i = 0; i < 10; i++) {
            thread[i] = new Thread(new Job(printQueue));
        }
        for (int i = 0; i < 10; i++) {
            Thread.sleep(1000);
            thread[i].start();
        }
    }
}
class Job implements Runnable{
    PrintQueue printQueue;
    public Job(PrintQueue printQueue) {
        this.printQueue = printQueue;
    }

    @Override
    public void run() {
        System.out.println("开始打印");
        printQueue.printJob(new Object());
    }
}
class PrintQueue{
    private Lock queueLock = new ReentrantLock(true);
    public void printJob(Object document){
        queueLock.lock();
        try {
            int i = new Random().nextInt(10) + 1;
            System.out.println("正在打印" + i/1000);
            Thread.sleep(i * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            queueLock.unlock();
        }
        queueLock.lock();
        try {
            Long duration = (long) Math.random()*10000;
            System.out.println("正在打印" + duration/1000);
        }finally {
            queueLock.unlock();
        }
    }
}

