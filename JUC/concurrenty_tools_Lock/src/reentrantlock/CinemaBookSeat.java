package reentrantlock;

import java.util.concurrent.locks.ReentrantLock;

/**
 * 演示多线程预定电影院座位
 * @author Tomorrow
 * @create 2021-09-02 19:04
 */
public class CinemaBookSeat {
    private static ReentrantLock lock = new ReentrantLock();
    private static void bookSeat(){
        lock.lock();
        try {
            System.out.println(Thread.currentThread().getName()+"开始预定座位");
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) {
        new Thread(() -> bookSeat()).start();
        new Thread(() -> bookSeat()).start();
        new Thread(() -> bookSeat()).start();
        new Thread(() -> bookSeat()).start();
    }
}
