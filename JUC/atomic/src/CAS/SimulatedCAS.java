package CAS;

/**
 * CAS等价代码
 *
 * @author Tomorrow
 * @create 2021-09-02 23:21
 */
public class SimulatedCAS {
    private volatile int value;
    public synchronized int compareAndAwap(int expectedValue,int newValue){
        int oldValue = value;
        if (expectedValue == oldValue){
            value = newValue;
        }
        return oldValue;
    }
}
