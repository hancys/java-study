package CAS;

/**
 * CAS等价代码
 *
 * @author Tomorrow
 * @create 2021-09-02 23:21
 */
public class TwoThreadsCompetition implements Runnable{

    private volatile int value;
    public synchronized int compareAndAwap(int expectedValue,int newValue){
        int oldValue = value;
        if (expectedValue == oldValue){
            value = newValue;
        }
        return oldValue;
    }

    public static void main(String[] args) throws InterruptedException {
        TwoThreadsCompetition r = new TwoThreadsCompetition();
        r.value = 0;
        Runnable target;
        Thread thread = new Thread(r);
        Thread thread2 = new Thread(r);
        thread.start();
        thread2.start();
        thread.join();
        thread2.join();
    }

    @Override
    public void run() {
        compareAndAwap(0,1);
    }
}
