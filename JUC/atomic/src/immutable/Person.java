package immutable;

/**
 * 不可变的对象，其他类也不可改变
 *
 * @author Tomorrow
 * @create 2021-09-03 16:06
 */
public class Person {
     final int age = 18;
     final String name = "Alice";
     //可变的，只有全部不可变，不可变的是线程安全的
     int score = 0;
}
