package immutable;

/**
 * 测试final能否被修改
 *
 * @author Tomorrow
 * @create 2021-09-03 16:07
 */
public class TestFinal {
    public static void main(String[] args) {
        Person person = new Person();
        //person.age = 18;  编译错误
    }

}
