package immutable;

/**
 * 演示两种情况，基本变量和对象
 * 线程争抢会发生错误，把变量放到方法内，情况就变了
 * @author Tomorrow
 * @create 2021-09-03 16:40
 */
public class StackConfinement implements Runnable{
    int index = 0 ;

    public void inThread(){
        int neverGoOut = 0;
        for (int i = 0; i < 1000; i++) {
            neverGoOut++;
        }
        System.out.println(neverGoOut);
    }
    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            index++;
        }
        System.out.println(index);
    }

    public static void main(String[] args) throws InterruptedException {
        StackConfinement stackConfinement = new StackConfinement();
        Thread thread = new Thread(stackConfinement);
        Thread thread2 = new Thread(stackConfinement);
        thread.start();
        thread2.start();
        thread.join();
        thread2.join();
    }
}
