package atomic;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

/**
 * 升级原子
 * @author Tomorrow
 * @create 2021-09-02 22:26
 */
public class AtomicIntegerFieldUpdateDemo implements Runnable{
    static Candidate tom;
    static Candidate peter;

    public static AtomicIntegerFieldUpdater<Candidate> scoreUpdate = AtomicIntegerFieldUpdater.newUpdater(Candidate.class,"score");

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            peter.score++;
            scoreUpdate.getAndDecrement(tom);
        }
    }

    public static class Candidate{
        volatile int score;
    }

    public static void main(String[] args) throws InterruptedException {
        tom = new Candidate();
        peter = new Candidate();
        AtomicIntegerFieldUpdateDemo r = new AtomicIntegerFieldUpdateDemo();
        Thread thread = new Thread(r);
        Thread thread2 = new Thread(r);
        thread.start();
        thread2.start();
        thread.join();
        thread2.join();
    }
}
