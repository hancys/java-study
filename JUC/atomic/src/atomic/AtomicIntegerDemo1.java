package atomic;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * AtomicInteger基本用法 不需要加锁 也可以保证线程安全
 * @author Tomorrow
 * @create 2021-09-02 21:54
 */
public class AtomicIntegerDemo1 implements Runnable{
    private static AtomicInteger atomicInteger = new AtomicInteger();
    public void incrementAtomic(){
        atomicInteger.getAndIncrement();
    }
    private static volatile int basicCount = 0;
    public void incrementBasic(){
        basicCount++;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            incrementAtomic();
            incrementBasic();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        AtomicIntegerDemo1 demo1 = new AtomicIntegerDemo1();
        Thread thread = new Thread(demo1);
        Thread thread2 = new Thread(demo1);
        thread.start();
        thread2.start();
        thread.join();
        thread2.join();
    }
}
