package future;

import java.util.Random;
import java.util.concurrent.*;

/**
 * @author Tomorrow
 * @create 2021-09-06 19:31
 */
public class GetException {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(20);
        Future<Integer> submit = executorService.submit(new CallableTask());
        try {
            submit.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }
    static class CallableTask implements Callable<Integer> {

        @Override
        public Integer call() throws Exception {
            throw new IllegalAccessException("�׳��쳣");
        }
    }
}
