package threadpool;

import java.util.concurrent.*;

/**
 * @author Tomorrow
 * @create 2021-08-30 21:06
 */
public class CachedThreadPool {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        /*ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(8,
                16,
                15,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<Runnable>(15),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.CallerRunsPolicy()
        );*/

        for (int i = 0; i < 100; i++) {
            executorService.execute(new Task());
        }
    }
}
