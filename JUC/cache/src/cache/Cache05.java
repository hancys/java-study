package cache;

import cache.computable.Computable;
import cache.computable.ExpensiveFunction;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

/**
 * 利用future避免重复计算
 * @author Tomorrow
 * @create 2021-09-06 20:24
 */
public class Cache05<A,V> implements Computable<A,V> {
    private final Map<A, Future<V>> cache = new ConcurrentHashMap<>();
    private final Computable<A,V> c;

    public Cache05(Computable<A, V> c) {
        this.c = c;
    }

    @Override
    public V compute(A arg) throws Exception {
        Future<V> f = cache.get(arg);
        if (f == null){
            Callable<V> callable = new Callable<V>() {

                @Override
                public V call() throws Exception {
                    return c.compute(arg);
                }
            };
            FutureTask<V> vFutureTask = new FutureTask<>(callable);
            f = vFutureTask;
            cache.put(arg,vFutureTask);
            vFutureTask.run();
        }
        return f.get();
    }

    public static void main(String[] args) throws Exception {
        Cache05<String, Integer> ex = new Cache05<>(new ExpensiveFunction());
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Integer compute = ex.compute("666");
                    System.out.println("第一次"+compute);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Integer compute = ex.compute("667");
                    System.out.println("第2次"+compute);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Integer compute = ex.compute("666");
                    System.out.println("第3次"+compute);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
