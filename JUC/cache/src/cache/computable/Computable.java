package cache.computable;

/**
 * @author Tomorrow
 * @create 2021-09-06 20:22
 */
public interface Computable<A,V> {
    V compute(A arg) throws Exception;
}
