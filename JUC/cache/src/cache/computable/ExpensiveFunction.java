package cache.computable;

/**
 * @author Tomorrow
 * @create 2021-09-06 20:23
 */
public class ExpensiveFunction implements Computable<String,Integer>{

    @Override
    public Integer compute(String arg) throws Exception {
        Thread.sleep(5000);
        return new Integer(arg);
    }
}
