package cache;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * @author Tomorrow
 * @create 2021-09-06 20:08
 */
public class Cache01 {
    private final HashMap<String,Integer> cache = new HashMap<String,Integer>();
    public Integer computer(String userId) throws InterruptedException {
        //先查找cache中有没有存入
        Integer result = cache.get(userId);
        //如果没有找到，存入cache
        if (result == null){
            result = doComputer(userId);
            cache.put(userId,result);
        }
        return result;
    }
    private Integer doComputer(String userId) throws InterruptedException {
        TimeUnit.SECONDS.sleep(5);
        return new Integer(userId);
    }

    public static void main(String[] args) throws InterruptedException {
        Cache01 cache01 = new Cache01();
        Integer computer = cache01.computer("13");
        System.out.println("第一次"+computer);
        computer = cache01.computer("13");
        System.out.println("第二次"+computer);


    }
}
