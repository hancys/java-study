package cache;

import cache.computable.Computable;
import cache.computable.ExpensiveFunction;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Tomorrow
 * @create 2021-09-06 20:24
 */
public class Cache03<A,V> implements Computable<A,V> {
    private final Map<A,V> cache = new HashMap<>();
    private final Computable<A,V> c;

    public Cache03(Computable<A, V> c) {
        this.c = c;
    }

    @Override
    public V compute(A arg) throws Exception {
        System.out.println("进入缓存");
        V result = cache.get(arg);
        if (result == null){
            result = c.compute(arg);
            cache.put(arg,result);
        }
        return result;
    }

    public static void main(String[] args) throws Exception {
        Cache03<String, Integer> ex = new Cache03<>(new ExpensiveFunction());
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Integer compute = ex.compute("666");
                    System.out.println("第一次"+compute);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Integer compute = ex.compute("667");
                    System.out.println("第2次"+compute);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Integer compute = ex.compute("666");
                    System.out.println("第3次"+compute);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
