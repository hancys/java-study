package com.hancy.gmall.service;

import com.hancy.gmall.bean.UserAddress;

import java.util.List;

/**
 * @author Tomorrow
 * @create 2021-08-28 14:41
 */
public interface OrderService {

    /**
     * 初始化订单
     * @param userId
     */
    public List<UserAddress> initOrder(String userId);
}
