<%--
  Created by IntelliJ IDEA.
  User: TomORrow
  Date: 2021/5/23
  Time: 23:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>--%>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h4>if测试</h4>
<hr>
<form action="coreif.jsp" method="get">
    <input type="text" name="username" value="${param.username}">
    <input type="submit" value="登录">
</form>

<%--判断如果提交的用户名是管理员 则登陆成功--%>
<%--<%--%>
<%--    if (request.getParameter("username").equals("admin")){--%>
<%--        out.print("d登陆成功");--%>
<%--    }--%>
<%--%>--%>
<c:if test="${param.username == 'admin'}" var="isadmin">
    <c:out value="管理员欢迎！"/>
</c:if>
<c:out value="${isadmin}"/>
</body>
</html>
