<%--
  Created by IntelliJ IDEA.
  User: TomORrow
  Date: 2021/5/22
  Time: 17:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--  <%@include file=""%>  提取公共页面--%>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%--将两个页面合二为一--%>
    <%@include file="common/header.jsp"%>
    <h1>
        网页主体
    </h1>
    <%@include file="common/footer.jsp"%>

    <hr>

<%--拿到文件，静态资源拿，拼接页面 本质还是三个--%>
    <jsp:include page="/common/header.jsp"/>
    <h1>
        网页主体
    </h1>
    <jsp:include page="/common/footer.jsp"/>
</body>
</html>
