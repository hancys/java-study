package com.kuang.servlet;

import com.sun.deploy.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;

//中文数据传递
public class CookiesDemo03 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("gbk");
        //服务器端从客户端获取
        Cookie[] cookies = req.getCookies();//返回数据，说明cookies可能存在多个
        PrintWriter out = resp.getWriter();
        //判断cookies是否存在
        if (cookies != null){
            //如果存在，解决办法
            out.write("你上一次访问的时间是：");
            for (int i = 0; i < cookies.length; i++) {
                Cookie cookie = cookies[i];
                //获取cookies的方法
                if (cookie.getName().equals("name")){
                    URLDecoder.decode(cookie.getValue(),"utf-8");
                    out.write(URLDecoder.decode(cookie.getValue(),"utf-8"));
                    System.out.println(URLDecoder.decode(cookie.getValue(),"utf-8"));
                }
            }
        }else {
            out.write("这是第一次访问本站");
        }
        Cookie cookie = new Cookie("name", URLEncoder.encode("请","utf-8"));
        resp.addCookie(cookie);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
