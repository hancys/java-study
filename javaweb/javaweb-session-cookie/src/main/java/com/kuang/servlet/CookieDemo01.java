package com.kuang.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

//保存用户上一次访问的时间
public class CookieDemo01 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //服务器来的时间，把时间封装成信件，下次带上
        //解决中文乱码
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("gbk");

        PrintWriter out = resp.getWriter();

        //服务器端从客户端获取
        Cookie[] cookies = req.getCookies();//返回数据，说明cookies可能存在多个

        //判断cookies是否存在
        if (cookies != null){
            //如果存在，解决办法
            out.write("你上一次访问的时间是：");
            for (int i = 0; i < cookies.length; i++) {
                Cookie cookie = cookies[i];
                //获取cookies的方法
                if (cookie.getName().equals("lastLoginTime")){
                    //获取cookies中的值
                    //cookie.getValue();
                    long lastLoginTime = Long.parseLong(cookie.getValue());
                    Date date = new Date(lastLoginTime);
                    out.write(date.toLocaleString());
                }
            }
        }else {
            out.write("这是第一次访问本站");
        }

        //服务器给客户端响应一个cookies
        Cookie cookie = new Cookie("lastLoginTime", System.currentTimeMillis()+"");
        cookie.setMaxAge(24*60*60);//COOKIES有效期为1天 秒为单位
        resp.addCookie(cookie);


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
