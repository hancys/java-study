package com.kuang.servlet;

import com.kuang.pojo.Person;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

public class SessinDemo01 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //解决乱码问题
        resp.setCharacterEncoding("utf-8");
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        //得到session
        HttpSession session = req.getSession();

        //给session中存东西
        session.setAttribute("name",new Person("情",1));

        //获取sessionID
        String sessionID = session.getId();

        //判断session是不是新创建的
        if (session.isNew()){
            resp.getWriter().write("session创建成功，ID："+sessionID);
        }else {
            resp.getWriter().write("session已经在服务器中！ID："+sessionID);
        }

        //session创建的时候做的事情
//        Cookie cookie = new Cookie("JSESSIONID",sessionID);
//        resp.addCookie(cookie);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
